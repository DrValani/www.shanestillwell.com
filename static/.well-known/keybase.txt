==================================================================
https://keybase.io/shanestillwell
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://www.shanestillwell.com
  * I am shanestillwell (https://keybase.io/shanestillwell) on keybase.
  * I have a public key ASDtLGEBWJK61erLkJy7cWH2kuBTDOoAiM2IBrxOf9Gsago

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01016009cdd89c3e32f0104cd5de19f4064c5177cbc3be65d9083b52b76a7eb59ab30a",
      "host": "keybase.io",
      "kid": "0120ed2c61015892bad5eacb909cbb7161f692e0530cea0088cd8806bc4e7fd1ac6a0a",
      "uid": "0a4a7f87ceedc425a7b3acddf8691700",
      "username": "shanestillwell"
    },
    "merkle_root": {
      "ctime": 1518968251,
      "hash": "21d7eccd910104647f058348e40921b3fb92cd3a58d2d706293b06ca7bdd2c6b07c318e0f6c60796dd54bb40d98a010b4b6141ee13f2376deaa52eaaef228e42",
      "hash_meta": "113c8699d6c147a5680edc2ef9f4498f51633b555d8be522fe7d059ce88d09f4",
      "seqno": 2119386
    },
    "service": {
      "hostname": "www.shanestillwell.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.40"
  },
  "ctime": 1518968268,
  "expire_in": 504576000,
  "prev": "4a8f10153ad2463a15f5098ca77011c72a186ec64917db30dcce7fa2e60d6b62",
  "seqno": 7,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg7SxhAViSutXqy5Ccu3Fh9pLgUwzqAIjNiAa8Tn/RrGoKp3BheWxvYWTFA1R7ImJvZHkiOnsia2V5Ijp7ImVsZGVzdF9raWQiOiIwMTAxNjAwOWNkZDg5YzNlMzJmMDEwNGNkNWRlMTlmNDA2NGM1MTc3Y2JjM2JlNjVkOTA4M2I1MmI3NmE3ZWI1OWFiMzBhIiwiaG9zdCI6ImtleWJhc2UuaW8iLCJraWQiOiIwMTIwZWQyYzYxMDE1ODkyYmFkNWVhY2I5MDljYmI3MTYxZjY5MmUwNTMwY2VhMDA4OGNkODgwNmJjNGU3ZmQxYWM2YTBhIiwidWlkIjoiMGE0YTdmODdjZWVkYzQyNWE3YjNhY2RkZjg2OTE3MDAiLCJ1c2VybmFtZSI6InNoYW5lc3RpbGx3ZWxsIn0sIm1lcmtsZV9yb290Ijp7ImN0aW1lIjoxNTE4OTY4MjUxLCJoYXNoIjoiMjFkN2VjY2Q5MTAxMDQ2NDdmMDU4MzQ4ZTQwOTIxYjNmYjkyY2QzYTU4ZDJkNzA2MjkzYjA2Y2E3YmRkMmM2YjA3YzMxOGUwZjZjNjA3OTZkZDU0YmI0MGQ5OGEwMTBiNGI2MTQxZWUxM2YyMzc2ZGVhYTUyZWFhZWYyMjhlNDIiLCJoYXNoX21ldGEiOiIxMTNjODY5OWQ2YzE0N2E1NjgwZWRjMmVmOWY0NDk4ZjUxNjMzYjU1NWQ4YmU1MjJmZTdkMDU5Y2U4OGQwOWY0Iiwic2Vxbm8iOjIxMTkzODZ9LCJzZXJ2aWNlIjp7Imhvc3RuYW1lIjoid3d3LnNoYW5lc3RpbGx3ZWxsLmNvbSIsInByb3RvY29sIjoiaHR0cHM6In0sInR5cGUiOiJ3ZWJfc2VydmljZV9iaW5kaW5nIiwidmVyc2lvbiI6MX0sImNsaWVudCI6eyJuYW1lIjoia2V5YmFzZS5pbyBnbyBjbGllbnQiLCJ2ZXJzaW9uIjoiMS4wLjQwIn0sImN0aW1lIjoxNTE4OTY4MjY4LCJleHBpcmVfaW4iOjUwNDU3NjAwMCwicHJldiI6IjRhOGYxMDE1M2FkMjQ2M2ExNWY1MDk4Y2E3NzAxMWM3MmExODZlYzY0OTE3ZGIzMGRjY2U3ZmEyZTYwZDZiNjIiLCJzZXFubyI6NywidGFnIjoic2lnbmF0dXJlIn2jc2lnxEB5uljU1eqj+hNhZlXc6dzws8nWXW8zOaEiAomTFb5AFLMuMLoN2n0+khZyFn9xrnu+G9H5bvZd6gIRJAzKM/kHqHNpZ190eXBlIKRoYXNogqR0eXBlCKV2YWx1ZcQgEXfetlkcXlbqUPlVQ23+TSDSwntICl98JmTwmabAB7WjdGFnzQICp3ZlcnNpb24B

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/shanestillwell

==================================================================
