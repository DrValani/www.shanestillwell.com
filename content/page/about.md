---
title: About Shane
date: 2010-12-07 15:16:41
---

# I'll make this brief

* **Name**: Shane A. Stillwell
* **What I do most days**: Javascript programming {React, Node.js, Angular}
* **What's important**: Family, everything else is a distant second

