---
title: "Rebuilding the iPhoto library"
id: 82
date: "2010-01-17T11:35:57-05:00"
tags: 
- Programming
- Apple
---

The number of photos I have in iPhoto is around 10,000\. I like the new faces feature, but it just kept grinding my CPU for hours and hours and days and days. I stumbled on a way to rebuild the iPhoto library, but it's really hidden and I've never run across this type of a setup from Apple Software.

Essentially you need to hold down Option + Command while starting iPhoto and keep them held down until you get a window asking you what you want to rebuild.  Interesting.  Maybe I'll report if solves the Faces issue with the CPU.

![HT2638_1.png](images/HT2638_1.png)

Reference:
http://support.apple.com/kb/HT263
