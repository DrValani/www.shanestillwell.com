---
title: "The One Desk to Rule Them All"
id: 620
date: "2013-03-24T21:00:42-05:00"
tags: 
- Good Ideas
---

It's time to give up the dedicated office and scale down to something a little smaller and more compact. I think I've struck a nice balance. Behold! I give you a renaissance of the **Drop Front Secretary Solid Wood Desk**. This desk has been in the family for a while. I think my grandparents had this desk in their house, with a few upgrades I've made into a modern convertible sitting desk + standing desk.

## Tame Sit Down Desk by Day

![](images/IMG_3538.jpg "IMG_3538")

## Vicious Standing Desk by Night

![](images/IMG_3539.jpg "IMG_3539")

