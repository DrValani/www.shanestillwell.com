---
title: "Zend Framework - Doctrine ORM strange Zend Tool behavior."
id: 113
date: "2010-06-26T05:50:43-05:00"
tags: 
- Programming
---

Zend Framework... awesome.
Doctrine ORB... awesome.

Both of these combined.... PURE AWESOME.

I set up a project just at it says to on this page http://github.com/fmntf/zf-doctrine. It took some doing, but I got it working and was quite happy with my initial results. Then I needed to start a new project, so I just copied all the files, tweaked the DB name was flying with a new project... with one caveat.

One of the modules I created using a YAML file was not getting created in the DB when I ran `zf build-project doctrine --reload`. It was frustrating, I tried changing a bunch of things.

In the end the whole problem was that the module folder didn't have a `controllers` folder and that was it. So I created a `controllers` folder and then it created the corresponding tables in the db. I think this code has much to do with it

application/Bootstrap.php
<!--?php class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {   /**    *  This will register all the module directories with ZF    */   public function _initModuleLoaders()   {       $this--->bootstrap('Frontcontroller');

$fc = $this-&gt;getResource('Frontcontroller');
$modules = $fc-&gt;getControllerDirectory();

foreach ($modules AS $module =&gt; $dir) {
$moduleName = strtolower($module);
$moduleName = str_replace(array('-', '.'), ' ', $moduleName);
$moduleName = ucwords($moduleName);
$moduleName = str_replace(' ', '', $moduleName);

$loader = new Zend_Application_Module_Autoloader(array(
'namespace' =&gt; $moduleName,
'basePath' =&gt; realpath($dir . "/../"),
));
}
}

}
