---
title: "Find JPG Files in Directory and CP (copy) to another directory"
id: 55
date: "2009-06-12T20:22:41-05:00"
tags: 
- Server Admin
---

Today I was faced with the challenge of finding all the JPG files in a a directory (and subdirecties of that) and copying them to another directory (striping out the subdirectory folder name).

`
find /path/to/directory/ -iname "*.jpg" -exec cp '{}' /path/to/dest/dir/ \;
`

That did it perfectly.

Keywords
Find Folder Strip Path Copy cp directory

Reference:
http://www.linuxquestions.org/questions/linux-newbie-8/shell-script-to-search-and-copy-jpg-to-another-folder.-730320/
