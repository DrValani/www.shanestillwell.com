---
title: "The SetEnv equivalent in Nginx for setting environmental variables."
id: 126
date: "2011-05-17T19:53:35-05:00"
tags: 
- Server Admin
---

If you need to pass some environment variables to your application from Nginx, you'll need to specify them in the config file like so.

`
    fastcgi_param APPLICATION_ENV staging;
 `

So for example a more full config for a Zend Framework application

`
server {
  listen 443 default ssl;
  listen 80 default;
  ssl_certificate 	/etc/pki/tls/certs/cert.crt;
  ssl_certificate_key 	/etc/pki/tls/private/cert.key;

  keepalive_timeout    70; 

  root /var/www/mysite/public
  access_log /var/log/nginx/mysite.access.log main;
  index index.php;

  location / {
    try_files $uri $uri/ /index.php?$args;
  }

  # set a nice expire for assets
  location ~* "^.+\.(jpe?g|gif|css|png|js|ico|pdf|zip|tar|t?gz|mp3|wav|swf)$" {
    expires    max;
    add_header Cache-Control public;
  }

  location ~* \.php {
    include fastcgi_params;
    fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
    fastcgi_read_timeout 600;
    fastcgi_pass  127.0.0.1:9000;
    fastcgi_param APPLICATION_ENV staging;
  }

}
`

There you have it. Go and do likewise.
