---
title: "Doctrine ODM MongoDB using an OR query"
id: 299
date: "2012-03-09T01:13:53-05:00"
tags: 
- Programming
---

This is no where in the documentation that I could find on Doctrine's site. I wanted to do search with a criteria of OR.

For example, in psuedo code

Find all Events where (first_name like 'Jim') OR (last_name like 'Jim') OR (title like 'Jim)

I searched, but the keyword of "OR" with Google doesn't yield very good results.

Thanks to Jamie Sutherland at
[http://www.jamiesutherland.com/2011/07/01/doctrine-2-odm-querybuilder-addor/](http://www.jamiesutherland.com/2011/07/01/doctrine-2-odm-querybuilder-addor/)

Here is how it's done

```php

$result = $this->_dm->createQueryBuilder('HOD\Model\Person');
$result->addOr($result->expr()->field('firstName')->equals(new \MongoRegex("/^{$query}/i")))
    ->addOr($result->expr()->field('lastName')->equals(new \MongoRegex("/^{$query}/i")));
return $result->getQuery()->execute();
```
