---
title: "Incorrect error message when enabling resources.session.save_path"
id: 122
date: "2010-11-30T00:00:00-05:00"
tags: 
- Programming
---

To customize the session location I added the following to my `application.ini` file.

`
resources.session.save_path = APPLICATION_PATH "/../var/session"
resources.session.remember_me_seconds = 604800 ;; One week
resources.session.gc_maxlifetime = 604800
`

After refreshing I encountered this error.

`
Zend_Session_Exception: session has already been started by session.auto-start or session_start() in /usr/local/zend/share/ZendFramework/library/Zend/Session.php on line 462
`

This was a little misleading as I looking through my app to see if the session.auto_start or if I had called session_start() in the `index.php` or `bootstrap.php`.... nothing.

Then just out of 'duh', I decided to see if the directory existed. Nope. 

### Solution

The error was completely misleading. The directory didn't exist. Created it and all was well.
