---
title: "Another great use for Evernote, tracking search results with screen shots."
id: 68
date: "2009-10-18T04:09:26-05:00"
tags: 
- Good Ideas
---

I have on occasion the necessity to track Google results of key terms for clients of mine. I usually grab a quick screen shot of the search results for that term. This is quite [simple](http://www.wikihow.com/Take-a-Screenshot-in-Mac-OS-X) on a mac.

## Here is where Evernote rocks

Instead of just random screen shots, I can drag or paste those screenshots into a note in Evernote. I could tag the note with key terms, but why bother? Evernote automatically finds the terms in the image for me.  Instead of having to keep track of screen shots by date, again Evernote does that for me too. Oh, and I can cram several different  screen shots into a single Evernote note.

## Recap

*   `⌘ + ⌃ + ⇧ + 4` gives me the option to place a screen capture on the clipboard, then I just paste it into an Evernote note.
*   Evernote indexes the text on that screen grab for me, making them easy to find later, tagging is optional
*   Evernote keeps track of the date
*   Evernote is awesome, couple it with the iPhone app, Jott, and you have one getting things done app

![Evernote](images/evernote2.png)
