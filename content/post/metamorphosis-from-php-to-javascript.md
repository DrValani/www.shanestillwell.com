---
title: "Metamorphosis from PHP to JavaScript"
id: 522
date: "2012-11-12T04:03:06-05:00"
tags: 
- ExpressJS
- MochaJS
- Node.js
- Programming
---

The following is account of why I prefer writing programs in JavaScript over PHP. I have well over 10 years of programming experience in PHP. I've written everything from a simple one page PHP program, to a complex [Zend Framework](http://framework.zend.com/) apps, to you name it, I've coded it in PHP (even stuff I know I should have not done in PHP).

> You Always Play To Your Strengths, if PHP is your strength, that's going to be your game

In the last few years I've created some web apps using [Node.js](http://nodejs.org), [AngularJS](http://angularjs.org) and started doing mobile application development with [Appcelerator](http://www.appcelerator.com/). These are all pure JavaScript offerings.

## 1 PHP was difficult to set up for local development

Getting Apache, MAMP, or Zend Server set up for local development is a chore. There is no getting around it. I develop on OS X and you have to edit the configs, install some packages, and more just to get a web server. This has changed with PHP 5.4 that includes a webserver, but there are still hurdles to get over

### Installing and running Node.js is easy

[Installing Node.js](https://github.com/joyent/node/wiki/Installation) is quite simple. Once you download and install you just tell node to run on the command line by `node server.js` where `server.js` is your main JS file.

Add to this, if it runs locally, it will run on [Heroku](http://heroku.com)

## 2 PHP was difficult to unit and integration test

I was able to do some unit testing in Zend Framework, but it was nothing less than a fight. Getting PHPUnit installed was the first round of this fight. Then setting up your testing environment is the second and most grueling round. _This is where PHP falls down flat, there are so many ways to do things, it's hard to find a straight one solution to fit all_.

Integration testing was not easy either, since I was using `Zend_Test_PHPUnit_ControllerTestCase` I was always running into issues that made it difficult to get it working properly.

### Unit testing and Integration testing in Node.js and ExpressJS

Thanks to [TJ Holowaychuk](https://github.com/visionmedia), unit testing with [MochaJS](http://visionmedia.github.com/mocha/) is pretty simple. I take that back, getting your head around it is a little of chore, but once you understand the difference between MochaJS (framework) and [ChaiJS](http://chaijs.com/) the actual tests, from there on out, it's dead simple to unit test business logic.

Integration Testing is awesome. Get this, [MochaJS](http://visionmedia.github.com/mocha/) employs [SuperTest](https://github.com/visionmedia/supertest) which uses [SuperAgent](https://github.com/visionmedia/superagent) all of these written by [TJ](https://github.com/visionmedia). It actually fires up a Node.js server on a random port to run the test and then shutsdown. Once you have it set up, it's really easy to write loads of API tests. Check out my dead simple [Makefile](https://gist.github.com/4057439)... you just need to type `make test` and it will run your tests (you can even have them run automagically when a file changes). Mind = Blown.

## 3 PHP does not lend itself to scaling

You're not Facebook... yes Facebook uses a type of PHP and scales great, but again you're not Facebook. Most likely, you're hosting your PHP using Apache and `mod_php` and haven't really tweaked your `httpd.conf` very much or done much with your `php.ini`. If this is the case, there's a strong chance that your app written in PHP will not scale to a second machine very elegantly.

### Enter Node.js.

Install app on Heroku, scale to infinity. This is how it works by default. Even if you didn't use Heroku, it's very easy to scale a Node.js app. This is because the app is pretty self contained. You could run many different Node.js instances behind a Nginx frontend.

## 4 PHP violates and obfuscates several factors in the 12 Factor App

The [12 Factor App](http://12factor.net) lays out really good advice to web apps for ease of management, development and scalability. Let's look at a few areas that many PHP apps by and large violate horribly.

1.  II Dependency

    PHP installs vary so much depending on which modules are install. I have never installed modules locally to satisfy the dependancy. They are ususally installed by PEAR in some common place on the server.

**Node.js** stores all needed modules locally in the app under the `node_modules` folder. And NPM is the bomb (does that still mean cool?)

2.  III Config

    I have yet to see a PHP app that stores config in environment variables. It's just not common practice at all. Take Zend Framework for example. It has the `application.ini` that uses grouping to store variant configurations. This is NOT scalable.

**Node.js** encourages the use of environment variables and is required when using Heroku.

3.  VII Port Binding

    Again if you're running PHP as an Apache `mod_php`, you are most likely wondering how you can bind PHP to a port. I would suggest kicking `mod_php` to the curb and running PHP as a FastCGI process with [PHP-NPM](http://php-fpm.org/). With this set up, PHP runs as a separate process that listens on a port. In this case you can have several backend servers running your PHP code, and one Apache/Nginx server doing a round robin to these PHP servers.

**Node.js** Can bind to whatever port you specify.

## 5\. Package Management or PEAR versus NPM

This is not even a fair fight. **PEAR** is _old and busted_, **NPM** is the _new hotness_. I cringe when I have to try to use PEAR to install stuff. First you have to make sure you have a `channel` installed in PEAR to get the packages that channel provides. Dependancy hell is another reason to loath PEAR.

NPM on the other hand is so easy to use. It's a central repo that anyone can upload modules to. All modules are stored in the local directory of that app so the app is entirely self contained. NPM also resolves dependancy like a boss.

    npm install express  

Done!!!

## The Bottom Line

Can you write complex, cool, fast, and awesome apps in PHP? Sure you can. Can you unit test, integration test, use environment variables, and satify local dependancies. Sure, but I've personally fallen prey to bad habits, forsaking unit testing, and more evils because it's convoluted to get it done in PHP.

It's been much easier for me to do all these things in Node.js and with **significantly less code**. I find that my code in JavaScript is MUCH more modular (code has a clean separation) which makes it much easier to unit test, use mocks and stubs.

## Ask Yourself: Could I write my next web apps in more than one language?

**Yes**: Good for your, you can appreciate the differences in different languages

**No**: I challenge you to write your next web app in something other than PHP (try Ruby on Rails, Django, Node.js, or _gasp_ .NET). Try anything other than a PHP.

## Resources
* http://www.codinghorror.com/blog/2012/06/the-php-singularity.html
* http://12factorapp.net
