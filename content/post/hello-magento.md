---
title: "Hello Magento"
id: 31
date: "2008-09-19T18:01:46-05:00"
tags: 
- Programming
---

I've been working with [Magento](http://www.magentocommerce.com/ "Magento - Home  - Open Source eCommerce Evolved"), the open-source ecommerce new kid on the block. Quite powerful for just having come on the scene as a new release 6 months ago.  It's developing at a rapid pace and uses OOP or the Zend Framework for it's base.

Having used [Xcart](http://www.x-cart.com/ "Shopping Cart Software &amp; Ecommerce Solutions: X-Cart. Free shopping cart trial is available.") extensively for over 4 years, this is quite refreshing.  I also got pretty aquatinted with [Ubercart](http://www.ubercart.org/ "Ubercart! A free open source e-commerce shopping cart") (Drupal Shopping Cart) having recently used it in a project that only has a dozen or so items and low volume. It's a nice drop in out of the box.

So as it stands here is how I rate the shopping carts (from least to best)

## [Xcart](http://www.x-cart.com)

It's pretty full featured, but is stuck back in the 2001 way of programing big projects. Pretty monolithic in its design. Some features in Xcart are pretty smart, others are just a slapped together bare bones effort. I'm moving away from Xcart and don't see any future in it.

## [Ubercart](http://www.ubercart.org)

This is a great, easy to manage, shopping cart with one caveat. It runs on [Drupal](http://www.drupal.org), Drupal is the best content management system I know, but has a steep learning curve. If you operate a site that is mostly a content site and you want to sell some things, this is package to go with. Drupal can do most anything, is developed at a break neck pace and has cool modules popping up all the time. Ubercart can handle basic products with colors and sizes and simple backend needs. It's user friendly and feels like the developers (Ryan and Lyle) have put good thought into making it a good experience for the customer.

## [Magento](http://www.magentocommerce.com)

This is the shopping cart to use if you just sell stuff. Just take a look at the [features](http://www.magentocommerce.com/features "Magento - Features  - Open Source eCommerce Evolved") and you'll agree, this is professional grade software. It still lacks gift certificate functions, but is really a breath of fresh air... stay tuned. 
