---
title: "Getting Jenkins ready for Node.js and AngularJS testing"
id: 604
date: "2013-01-15T03:02:09-05:00"
tags: 
- AngularJS
- Jenkins
- MochaJS
- Node.js
- UnitTesting
- Programming
---

Setting up Jenkins for Node.js + Mocha testing and such

Install this stuff

* Node.js via [Nave](https://github.com/isaacs/nave)
    * `git clone https://github.com/isaacs/nave.git`*   `./bin/nave.sh usemain v.0.8.9`
* Mocha
  * `npm install -g mocha`
* ChaiJS
  * `npm install -g chai`
* xunit-file (Mocha custom report for Jenkins)
  * `npm install -g xunit-file`
* Jshint
  * `npm install -g jshint`
* Nock
  * `npm install -g nock`
* Supertest
  * `npm install -g supertest`
* Testacular
  * `npm install -g testacular`
* PhantomJS
    * `npm install -g phantomjs`

Here is a handy script that will install v0.8.9 and all the others
https://gist.github.com/4535632
