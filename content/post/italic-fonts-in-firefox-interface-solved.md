---
title: "Italic fonts in Firefox Interface [solved]"
id: 89
date: "2010-02-02T03:51:58-05:00"
tags: 
- Apple
---

A coworker had this problem. After installing a plugin-update, the fonts on her Firefox interface were all italic.  We combed over the preferences to no avail, then a simple Google search revealed the answer.

1.  Open Font Book
2.  Find Lucida Grand Regular
3.  Right Click > Reveal in Finder
4.  Delete it. AAHHHHH!!!!  Yes, I'm sure, delete it,
5.  Witness the true power of a fully operational Mac Station, it suddenly recreates the font for you. Amazing

Now of course you need to restart Firefox, and everything was cool.

Credit:
http://www.stucel.com/blog/firefox-bold-font-ui-problem-snow-leopard
