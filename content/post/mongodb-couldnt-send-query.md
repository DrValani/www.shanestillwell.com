---
title: "MongoDB \"Couldn't Send Query\""
id: 305
date: "2012-03-18T18:43:55-05:00"
tags: 
- MongoDB
- Server Admin
---

I would get random error messages from my MongoDB based app [GoScouter](http://goscouter.com) of
<pre>couldn't send query</pre>
It was random and quite frustration. I was not able to find anything definite online about such error, but that it might be a bug in the PHP MongoDB Driver. I was using version 1.2.7, so I did a `pecl upgrade mongo` on the web server (using PHP-FPM) and this seemed to fix the problem. Now I'm running version 1.2.9 and I have not received a `Couldn't Send Query` error yet.
