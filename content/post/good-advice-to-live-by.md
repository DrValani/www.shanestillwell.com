---
title: "Good Advice to Live By"
id: 20
date: "2008-06-25T15:23:26-05:00"
tags: 
- Good Ideas
---

Jeff Atwood has a great [Coding Horror: The Ultimate Code Kata](http://www.codinghorror.com/blog/archives/001138.html) filled with some really good references even though the post seems to be targeted at programming, the references are much more broad than that. In summary, he admonishes his readers to 'effortful study' of their domain.

## Effortful Study

_This can mean many things, but let me summarize what it I've learned so far in life. It's not just enough to do something to be really good at. You have to actually be a student of that domain. Know the history, study those that are good, learn from those that went down in flames. Know the theories and how they interact and oppose each other. You have to try new things, you should be confused much, fail often, and back track repeatedly. Quitting is not an option._

_It all goes back to being proactive in that field rather than just coast._

&nbsp;

## Practical Advice

Little grasshopper, are you looking for ways to become the ninja in your field? Here are some ways to get there.

1.  Don't be in a hurry. Read [Teach Yourself Programming in Ten Years](http://www.norvig.com/21-days.html &quot;Teach Yourself Programming in Ten Years). It applies to programming, but in short, it also applies to all areas of profession. This is hard for those in their early twenties to swallow. I remember being gung ho about several things in my twenties and wanted to be the expert in those fields NOW. It doesn't work that way. This is a marathon, not a sprint.
2.  Set aside time each week for personal improvement. Google made this practice famous, but everyone can practice it in their field. What should you improve on?

    1.  Make a list of the tools you use in your field (i.e. software, hardware, maybe even people skills) and then spend some time getting to know those tools. Yeah sure, you know how to use them, but how do other people use them? Are their hidden features, shortcuts, substitutes? Know your tools in and out.
    2.  Read books. I don't mean fiction. I like books that stand the test of time. Read popular books written twenty years ago and are still highly recommended. Read how to books on the tools for your job (see #1).
    3.  Practice, Practice, Practice.

3.  Jump out of your field completely. Study some other area helps to create parallels between two fields. It gets you out of the forest of your expertise and you can then see the trees.

    1.  Study a foreign language. Do you speak English (yeah stupid question you're reading this blog), is that all you speak? My friend go out and learn another language and you will find it enriches your life. Many of the brilliant people in history spoke several languages, in fact, when we survey history, it's a rare thing one language to have such a hold as English (yes, Greek was widely used back in Jesus' day).
    2.  Jump to the other side of your brain. If you are a computer geek like me, then it's good to do something artsy. It hurts and is hard and doesn't make sense, but it will make you better at the things you really love, trust me.
