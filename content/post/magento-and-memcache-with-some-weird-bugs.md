---
title: "Magento and Memcache with some weird bugs"
id: 81
date: "2010-01-13T13:37:23-05:00"
tags: 
- Memcache
- Server Admin
---

When I launched some larger sites on Magento in August 2009 I was sold on the slight performance increase that [memcache](http://memcached.org/ "memcached - a distributed memory object caching system") provided the cart.  However, I ran into a few bugs and other anomalies that have caused me to abandon the use of memcache with Magento.  

Please note, it could be my implementation that was causing the problem since it was my first foray into memcache.

Two problems Magento and Memcache caused

1.  Refreshing the cache under 'Cache Management' didn't seem to take effect on all cache types. The layout, for instance, would not get refreshed. So if I made changes to the layout.xml files I would have to restart memcache manually to refresh the layouts.
2.  All the session data was saved in memcache as well, so when you restarted memcache to address the problem above, all logins, shopping carts, and any session data were gone.  Image painstakingly putting a dozen items in your cart to have them disappear for no reason... lost sale? You betcha!
3.  Since I'm on a roll, I'll just lay any other weird problem in Magento on memcache as well. Such as the unexplainable new item in my cart that I definitely didn't add myself. This happened once in a while to me (2 times a month of normal testing), but we never received any complaint about it.

As I stated, there is nothing wrong with Memcache, but I think there are a few bugs in the Magento implementation of the memcache default setup.
