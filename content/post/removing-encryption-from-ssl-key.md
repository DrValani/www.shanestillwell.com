---
title: "Removing Encryption from SSL Key"
id: 61
date: "2009-07-18T17:43:34-05:00"
tags: 
- Server Admin
---

While having an encrypted key is a good thing for an SSL web server, it just doens't work out so well. When your web server restarts, it hangs during startup asking for a passphrase for all the SSL certs.

To remove the requirement to enter a password you can follow these steps.

First backup you key
`
# cp www.domain.key www.domain.key.encrypted
`

Then create a plain text version
`
# openssl rsa -in www.domain.key -out www.domain.key.plain
`

Then replace your current key with the new plain text one
`
# cp www.domain.key.plaintext www.domain.key
`

Now restart your web server and you shouldn't be prompted for a passphrase.

## VERY IMPORTANT!!!!!

You were about to walk away were'nt you?! Well make sure that your `www.domain.key` and `www.domain.key.plain` files are are owned by `root` and are read/write only by root `chmod 600 www.domain.key`
