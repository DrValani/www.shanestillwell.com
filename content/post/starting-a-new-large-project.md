---
title: "Starting a New Large Project"
date: 2018-08-17T09:42:00-04:00
tags:
- Projects
---

Here I want to talk about what a team should do when they start a new project. I've had the privilege to architect a large project and to come into several existing projects that have a fair amount of girth. These just a few thoughts about things that need to be established sooner rather than later.


## Create a glossary (terms that everyone can agree on)

This is good for new comers to the project, getting their head around jargon. Create a place, or even in the README of every repo.

## Decide on naming conventions (npm modules, git repos, filenames)


> There are 2 hard problems in computer science: cache invalidation, naming things, and off-by-1 errors.  
> - - many people on the internet

Yeah, naming things. Seems easy, but a bad name can be confusing, or worse yet, having two or more names for a single idea. I project I was on had this, for example, to reference a "Movie" (e.g. Star Wars), the words **title**, **feature**, **movie**, **program**, could all mean a Movie. (╯°□°）╯︵ ┻━┻

### Files and folders

Once I was troubleshooting an issue that worked fine locally, but would totally fail on the server. It was a filename case issue. On the Mac, `MyFile.js`, worked fine, but on linux it failed, because the code was looking for `myfile.js`. Do youself a favor with files. Follow these simple rules

1. lower case
2. use hyphens (also known as kebab-case)
3. letters and numbers
4. No spaces what-so-ever

```
// BAD
MyFiles.js
This Really Bad Name For A File.js
lame_looking_files.js

// Good
my-files.js
this-really-good-name-for-a-long-file.js
better-looking.js
```

## Decide on syntax / linter

Developers have opinions, I'm no different, and if you're honest, you have opinions too. The problem comes when we have opinions on things that have little actual value. It's called [Bike Shedding](https://en.wiktionary.org/wiki/bikeshedding), the technical stuff that matters usually receives little argument, but the color of the shed, or tabs versus spaces, or to semi-colon or not-semi-colon ;)  .

Developers like to get opinionated about such things as where `{ }` brackets go, spacing, if you should use a certain coding structure. It's insane, and in Javascript land, it's down right mindblowing.


Free yourself, leave all that stuff behind. Let go of your opinions. Just rely on [one](https://github.com/airbnb/javascript), of [the](https://google.github.io/styleguide/jsguide.html) [many](https://github.com/standard/standard) already well established style guides. Just as a note, a pretty greenhorn move is committing a bunch of changes your editor made automatically.

> If you editor is making updates to the code automatically, then fix your editor or get a new one.

So put linting at the first thing that's done by your CI when a PR is opened. If it doesn't pass linting, then fail it immediately.

On the other side of the fence, just because you pick a popular style, don't be afraid to tweak it a little, but careful that you don't go overboard. As an example, I'm a big fan of the [dangling comma]( https://eslint.org/docs/rules/comma-dangle) rule. To me it makes sense to just allow it since Javascript has no issue with it, and it makes cleaner git diffs.

## .... More to Come ....
