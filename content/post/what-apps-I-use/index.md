---
title: "Apps I use and may be useful to you too"
date: 2018-06-02T08:55:47-05:00
tags:
- Apple
- Programming
- Vim
- Postgresql
- Docker
---

> A run down on the apps I use regularly and why. Caution, Mac specific drivel ahead.

## Vim

Do I even need to explain? It's pretty common place for proud Vim users to show off their `.vimrc`, but I've come to find copy/paste from someone else's `.vimrc` is not really a good idea. A developer's `.vimrc` is a personal matter, crafted over time with love and affection, it suits them and their needs.

I run Vim from [iTerm2](https://www.iterm2.com), installed from [Homebrew](https://brew.sh/). Back in the day, I used to use [MacVim](https://github.com/macvim-dev/macvim), which has a few niceties, but not really needed with what iTerm provides me.

---

## iTerm2

https://www.iterm2.com/

> The default macOS Terminal.app is pretty much useless

![iTerm2](images/iterm2.png "Iterm Term split panes")

iTerm has been my goto terminal for as long as I've been on a Mac. Awesome features include **profiles**, **split planes**, **tabs**, **full screen windows**, **search**, **auto copy**, and a ton of other features I don't even use.

---

## Dash

https://kapeli.com/dash

> Fast, offline documentation for almost anything

Yeah, you could go online to look at the documentation. Rely on Google, take time to click around. In this instance, it's much faster to use Alfredapp + Dash to search for the documentation set (e.g. Javascript, CSS, Redis, Vue, Postgres). Their list of supported documentation is pretty impressive and you can make your own if want to as well, but I've never had to.

In this screen cast you see me use Alfred. For instance I would type `Ctrl + Space` to bring up Alfred, then type `java` and you see Alfred suggests Dash's Javscript doc set, so I press `tab` to autocomplete. Then from there I can type anything in Javascript doc set and it will suggest matches. You see me type `array.is` and I can get to my documentation very quickly.

![Dash](images/dash.gif "Dash in action")

> Never memorize something that you can look up   
> ― Albert Einstein

*Free Alternative*: https://devdocs.io/

---

## Private Internet Access (PIA)

https://www.privateinternetaccess.com/

> Stay safe out there

A mobile warrior will find themselves connecting to all sorts of different Wifi available. Much of it is not encrypted (e.g. Starbucks) so all your traffic is broadcast to everyone. Many websites and services use HTTPS / SSL these days, so someone sniffing the content of your traffic is not as much of a concern, but they can see where you go, and even mess with your connection. Sometimes important ports are blocked, such as SSH port 22. I need those to do my work.

To protect myself and get around these Orwellian measures I use [Private Internet Access](https://www.privateinternetaccess.com/). For only $50 a year, I can connect my Macbook Pro, iPhone, and iPad to PRI and the traffic from my computer to their VPN is encrypted and escape the prying eyes of your ISP and other net neutrality issues. Bonus tinfoil hat points if you go to Walmart, buy a gift card with cash, use a throw away email account from [Shark Lasers](https://www.sharklasers.com/), and sign up for an account. Now the NSA can't track your nefarious activity online. At least that is the idea. Maybe it's just a pipe dream and everything I do is easily thwarted by the NSA 🤷.

![Private Internet Access](images/pia.png "PIA Settings")

### Pros

* Can use one account and connect multiple devices
* Verify flexible connection options (i.e. you can make it look like you're making a lot of traffic to some website)
* Many different options to connect all over the world (i.e. make Google think you're coming from France, not sure you would want to though)

### Cons

* Many captive portals have blocked access to PIA making it hard to connect sometimes
* The network is slow sometimes. It's usually solved by reconnecting, but it's a pain when the unencrypted connection is fast, but connecting to PIA slows your connection down.

---

## Choosy

https://www.choosyosx.com/

![Choosy](images/choosy.png "Choosy")

I stumbled upon this little Preference Pane app a little while back when I wanted to open various links in different browsers for reasons I forget. It's a paid app ($10 at the time of this writing). It's pretty simple, Choosy becomes your default browser and when you try to open a link in email, or any other app, it'll prompt you which browser. It's both nice and annoying. Sometimes I'll forget once I click a link, I also have to click the browser icon (remember the days of double click 👴)

---

## TripMode

https://www.tripmode.ch/

![Trip Mode](images/tripmode.png "TripMode screenshot")

This little gem is handy when you want to limit the traffic on your hotspot. For example, if you don't want backups happening on your hotspot or if you want to limit something otherwise would suck up the network. The one problem I always have, if I have it on, I forget I need to enable each process to access the network. This means I'll be scratching my head wondering why something didn't work (in fact it happened right now while writing this and wondering why I wasn't getting messages through Cisco Teams). I'm my worst enemy.

As an added bonus, you can spy on processes using the network and possibly deny them.

---

## Arq

> Offsite Backups

https://www.arqbackup.com/

I've tried them all, [Backblaze](https://www.backblaze.com/), [Crashplan](https://www.crashplan.com/en-us/), and [Carbonite](https://www.carbonite.com/). All of them work fine, but my biggest rub was **security**. These are my files, and I want them encrypted before they are sent offsite, and only I hold the key. Backblaze does a good job at this, but they fall flat on their face when you want to restore a file, you need to give them the encryption key 😮.

![Arq](images/arq.png "Arq file / folder settings")

### Pros

* Backend agnostic, feel free to store your files on S3, Azure, Backblaze B2, and more.
* Encrypted before sent off to remote server
* One fee for the software
* Can decide which WiFi networks to use (or not use)

### Cons

* Searching for files currently sucks. You need to know the exact file you want to restore.
* In my opinion, the algorithm for including files is a little flawed. For example, if you include your `~/` HOME directory, and exclude some items from the HOME directory, say for example `~/.npm`. The problem comes when a new file/folder is added to your HOME directory, it's automatically included in backups. I can see this being bad either way. You're either going to say *"Hey, I don't want that folder backed up, but it automatically added it"*, **OR** *"Shoot, I thought that was being backed up"*.
* I know I listed excluding wifi as a *pro*, but the problem here again is a flawed algorithm. If you select to exclude some wifi networks (like your tethered phone), then any new wifi will also be excluded by default. I know, I'm hard to please.

--- 

## Postico

https://eggerapps.at/postico/

> A simple Postgresql GUI

I know the real PG pros use `psql` for all their PG queries, so I guess I'm not pro. Postico is a paid product, but worth it IMHO if you are doing plenty of SQL queries a day. It supports tabs, which is great for keeping track of values in different tables. I like the SQL editor, it has autocomplete, plus it's really handy to paste in queries.

![Postico](images/postico.png "Postico Tabs and SQL editor")

### Pros

* Have a ton of saved PG servers you can connect to saved as favorites (color coded too)
* Auto populates the connection details from a `postres://...` connection string in the clipboard, cool
* Multiple tabs

### Cons

* Is mostly used for Postgres usage, not really for permissions or administration
* Can't easily see functions, triggers, or other more hidden features in a DB

---

## DropShare

https://getdropsha.re/

> Share files using your own storage provider

Keeping with the theme, *I like to be in control of my data*, I found Dropshare a few years ago and have really enjoyed using it to share files, screen shots, and screen recordings. I've found being able to share screen shots quickly is paramount when working remote.



Best of all, I can upload to S3 and use a domain I own in Route53, then utilize CloudFront + SSL. What am I talking about? You should check out [this post]({{< ref "/post/Using-free-SSL-and-Cloudfront-for-an-Angular-React-site.md" >}}) I made setting up S3, CloudFront, SSL, Route53. Check out these links I used Dropshare to create.

* https://x.s12l.com/GNnxdKxdXQ.gif
* https://x.s12l.com/C8ey3358GR.png
* https://x.s12l.com/rjxsa5D1Fk.mp4


---

## Honorable Mentions

Most of these are covered elsewhere or are in fact no-brainers.

* [Alfredapp](https://www.alfredapp.com/) Launcher, search, clipboard history, and much more.
* [Evernote](https://www.evernote.com) I usually stuff anything / everything in there.
* [Clean My Mac](https://macpaw.com/cleanmymac) Nice for cleaning up your Mac.
* [ScreenFlow](https://www.telestream.net/screenflow/overview.htm) Profession screencasting.
* ~~[Spark](https://sparkmailapp.com/) I like trying new email clients from time to time, this is my current favorite.~~
    * **Update**: Apps like Spark need to make money, and many times they choose to use their user's data to do it. This is unfortunate. When you give your email credentials to a client app like Spark, they store that and can then download your email for their purposes. You have to trust them. On the other hand, having multiple email accounts and using a web browser for each is not ideal either. So for now I've switched to using Mail.app on the computer/phone. I trust Apple is not selling my information.
    * https://www.reddit.com/r/privacy/comments/5grsan/do_not_use_the_spark_email_client_by_readdle/
* [Docker for Mac](https://www.docker.com/docker-mac) Docker ALL. THE. THINGS.
* [Harvest](https://www.harvestapp.com) Time tracking.
* [Fantastical](https://flexibits.com/fantastical) I've used it for years, handy.

