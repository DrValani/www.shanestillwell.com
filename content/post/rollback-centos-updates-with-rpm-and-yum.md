---
title: "Rollback CentOS updates with RPM and YUM"
id: 112
date: "2010-06-14T19:21:02-05:00"
tags: 
- Server Admin
---

Every server administrator likes to sleep at night and every server admin knows server updates could prove disastrous. Just found out that RPM and Yum allow you the ability to rollback an update if some how it sent your system into a choking fit.

In your `/etc/yum.conf` add this in there somewhere
`
tsflags=repackage
`

This will repackage all your old files from the package (including config files) and store them in `/var/spool/repackage`

Also add the file `/etc/rpm/macros` and place this in that new file
`
%_repackage_all_erasures 1
`

## When things go wrong

If you want to rollback changes to a certain time, then just issue commands like

`
rpm -Uhv --rollback '9:00 am'
rpm -Uhv --rollback '4 hours ago'
rpm -Uhv --rollback 'december 25'
`

## Untested Yet

This is just stuff I'm reading, I have yet to test this under fire, maybe I'll play around on a test server

## Resources

http://dailypackage.fedorabook.com/index.php?/archives/17-Wednesday-Why-Repackaging-and-Rollbacks.html
