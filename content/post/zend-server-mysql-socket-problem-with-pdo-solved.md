---
title: "Zend Server Mysql Socket problem with PDO [Solved]"
id: 106
date: "2009-11-30T00:00:00-05:00"
tags: 
- Server Admin
---

## The Problem

This answer has eluded me for months. I have Zend Server CE installed on my Mac (and linux web servers). I would use a DSN similar to the following (please note that my real password is not 'root').
<pre>mysql://root:root@localhost/thedatabase</pre>
This would result in an error in a PHP Fatal error of
<pre>'Can't connect to local MySQL server through socket '/tmp/mysql.sock''</pre>
This drove me mad looking for an answer. I had not configured it to connect through a socket, why was it doing that? To add more injury to this insult, if I tried to connect to Zend Server's mysql socket (/usr/local/zend/mysql/tmp/mysql.sock) I would still get the same error message.

## The Best Solution

It's as simple as specifying `127.0.0.1` in replace of `localhost`. I guess if you do this, you force PDO to use a TCP connection.

## Solution Alternative

I haven't tried this, but you could also symlink from
<pre>/tmp/mysql.sock</pre>
to
<pre>/usr/local/zend/mysql/tmp/mysql.sock</pre>
.

I'm not a big fan of symlinks that reach outside of the application.

## Cause

It seems there is a bug in PDO PHP 5.2.x that is not fixed until 5.3.x.

Resources:
http://forums.zend.com/viewtopic.php?f=44&amp;t=568
