---
title: "Drupal Mimemail missing embedded images [Solved]"
id: 78
date: "2010-01-07T02:42:22-05:00"
tags: 
- Programming
---

A really weird bug in Drupal's [Mime Mail](http://drupal.org/project/mimemail "Mime Mail | drupal.org") module. This module allows you to send HTML emails with links to images and such. 

## Behavior

Create a newsletter, send a test one, works great.
Send it for real and the embedded images are missing.

I initially thought it was poormanscron, but I don't have that installed on this Drupal site. Basically what was happening is the first email would be the embedded image (hence it worked when you tested it) but all subsequent recipients would not get the image.

The fix is a simple change to `mimemail.inc` here is the diff file
`
  --- mimemail.inc	2009-04-01 18:09:50.000000000 +0200
  +++ mimemail.inc	2009-04-01 18:10:06.000000000 +0200
  @@ -148,6 +148,7 @@ function _mimemail_file($url = NULL, $na

     $ret = $files;
     $files = array();
  +  $filenames = array();
     return $ret;
   }
`

Credit:
http://drupal.org/node/358439#comment-1426276
