---
title: "Magento and setroubleshoot causing certain pages to die."
id: 71
date: "2009-12-04T20:40:36-05:00"
tags: 
- Server Admin
---

Some pages in Magento were giving just a white page, with no indication of errors in Zend Server console or log files.  This was apparently a memory issue, so I restarted the web server and all was fine.

One issue I noticed was the server "setroubleshoot" was taking 12% of the systems memory.  This program just logs issues with SELinux and it seems SELinux doesn't like my Magento install.  I have SELinux just set to 'warn', but I may want to disable it all together.  I stopped the 'setroubleshoot' program and turned it off with chkconfig.
