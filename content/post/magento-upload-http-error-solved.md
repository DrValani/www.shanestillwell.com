---
title: "Magento Upload HTTP Error [SOLVED]"
id: 110
date: "2010-06-03T16:49:47-05:00"
tags: 
- Programming
---

This was a strange error with Magento. I was in a product, trying to upload an image. The image would upload fine, but then error out with "Upload HTTP Error". I changed permissions up and down the `/media` and `/var` directories and made them as loose with permissions as I could.

Still no change.

Then I happened upon this comment in the forums.

<quote>To anyone that has a HTTP upload error AND has a password protected store directory (for under construction reasons): 
remove the directory password protection and you’ll be able to upload your files again! That was what happened to me.</quote>

BINGO!

I removed the password protection for the admin (not the Magento password, but Apache basic password protection).

Reference:
http://www.magentocommerce.com/boards/viewreply/154831/
