---
title: "Using Testacular with Jenkins for AngularJS e2e Testing"
id: 598
date: "2013-01-09T17:19:46-05:00"
tags: 
- Programming
- AngularJS
- Jenkins
---

# Rollcall!

* AngularJS in your code ✓
* Jenkins ✓
* [Node.js is installed]({{< relref "/post/installing-node-js-on-centos-using-nave.md" >}}) on the Jenkins server? ✓
* Testacular is installed on the Jenkins server ✓*   PhantomJS is installed on the Jenkins server &#10003;

If that's what you're using, this is how I set up testing on the Jenkins server.

1. Create a new job in Jenkins
2. Pull from Git (or whatever), Trigger how you want too. That's not germane to this article.
3. Inject some environment variables (may need to install a plugin for this)
   ![Env Vars](images/Screen-Shot-2013-01-09-at-10.35.05-AM.png)
4. Start a web server on the Jenkins server for Testacular to use and call the target in the `build.xml` script (see below)
   ![Web Server](images/Screen-Shot-2013-01-09-at-10.44.58-AM.png)
5. Generate the report (test_out/e2e.xml is configured in testacular-e2e.conf.js)
    ![xunit](images/Screen-Shot-2013-01-09-at-11.12.27-AM.png)

## build.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project name="My Project" default="build" basedir=".">
    <target name="testacular-e2e" description="Testacular AngularJS e2e Tests">
        <echo message="Running the tests ..." />
        <exec executable="testacular" output="test_out/output.txt" failonerror="true">
            <arg value="start" />
            <arg value="./config/testacular-e2e.conf.js" />
        </exec>
        <echo message="Tests done" />
    </target>
</project>
```

## config/testacular-e2e.conf.js

```javascript
basePath = '../';

files = [
  ANGULAR_SCENARIO,
  ANGULAR_SCENARIO_ADAPTER,
  'test/e2e/**/*.js'
];

autoWatch = false;

browsers = ['PhantomJS'];

reporters = ['dots', 'junit'];
singleRun = true;

proxies = {
  '/': 'http://localhost:8000/'
};

junitReporter = {
  outputFile: 'test_out/e2e.xml',
  suite: 'e2e'
};
```
