---
title: "Setup PHPunit for testing Zend Framework application"
id: 87
date: "2010-02-02T01:27:07-05:00"
tags: 
- UnitTesting
- Programming
---

This was how I set up PHPUnit with my Zend Framework application.  I'm very new to testing and have never set it up before.

1.  Need to get PHPunit installed on the Apple Mac
Try typing this in the command line (btw I prefer [iTerm](http://iterm.sourceforge.net/) over the built in terminal). 
`
whereis phpunit
`

I tried a few different ways to install php, I'm not sure which one eventually installed it, but.... yeah I know sounds dumb, but when you try a bunch of different ways and then it 'just starts working' you lose track which way actually fixed it.  Besides it was two weeks ago, I can't remember that far back. I do know that one of the has Xdebug turned on to generate the nice reports of code coverage. 

For example.
`
Shane:tests shane$ whereis phpunit
/usr/bin/phpunit
Shane:tests shane$ phpunit --version
PHPUnit 3.4.6 by Sebastian Bergmann.

Shane:tests shane$ 
`

2.  Now setup your environment. This is the hard part, since everyone does it different.  The trouble is figuring out which pieces need to go where and why.  

Bonus
This will create a neat little tree of your folder structure
`
ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/   /' -e 's/-/|/' 
`

So I have this structure for my application
`
   |-application
   |---configs
   |---controllers
   |---models
   |-----DbTable
   |---views
   |-----helpers
   |-----scripts
   |-------error
   |-------index
   |-library
   |-public
   |-tests
   |---application
   |-----controllers
   |-----models
   |---library
   |---log
   |-----report
`

Notice the "test" folder with subfolders? Good.

We now have the necessary folder, here are files

tests/phpunit.xml
`
<phpunit bootstrap="./bootstrap.php" color="true">
    <testsuite name="My Test Suite">
        <directory>./</directory>
    </testsuite>

    <filter>
        <whitelist>
            <directory suffix=".php">../library/</directory>
            <directory suffix=".php">../application/</directory>
            <exclude>
                <directory suffix=".phtml">../application/</directory>
            </exclude>
        </whitelist>
    </filter>
    <logging>
        <log type="coverage-html" target="./log/report" charset="UTF-8"
            yui="true" highlight="true"
            lowUpperBound="50" highLowerBound="80"/>
        <log type="testdox-html" target="./log/testdox.html" />
    </logging>

</phpunit>
`

tests/bootstrap.php
<php>
<?php

error_reporting( E_ALL | E_STRICT );
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);

define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
define('APPLICATION_ENV', 'testing');
define('LIBRARY_PATH', realpath(dirname(__FILE__) . '/../library'));
define('TESTS_PATH', realpath(dirname(__FILE__)));

$_SERVER['SERVER_NAME'] = 'http://local.amazon.com';

$includePaths = array(LIBRARY_PATH, get_include_path());
set_include_path(implode(PATH_SEPARATOR, $includePaths));

require_once "../library/Zend/Loader/Autoloader.php";
#Zend_Loader::registerAutoload();
$loader = Zend_Loader_Autoloader::getInstance();
$loader->setFallbackAutoloader(true);
$loader->suppressNotFoundWarnings(false);

Zend_Session::$_unitTestEnabled = true;
Zend_Session::start();

require_once 'application/ControllerTestCase.php';
</php>

tests/ControllerTestCase.php
<php>
<?php

/**
     * Description of ControllerTestCase
 *
     * @author shane
 */
class ControllerTestCase extends Zend_Test_PHPUnit_ControllerTestCase
{
    public $application;

    public function setUp()
    {
        $this->application = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini'
        );

        $this->bootstrap = array($this, 'appBootstrap');
        parent::setUp();
    }

    public function tearDown()
    {
        Zend_Controller_Front::getInstance()->resetInstance();
        $this->resetRequest();
        $this->resetResponse();

        $this->request->setPost(array());
        $this->request->setQuery(array());
    }

    public function appBootstrap()
    {
        $this->application->bootstrap();
    }
    //put your code here
}
</php>

3.  Create an example test

test/models/StatsTest.php
<php>
<?php

class Model_StatsTest
    extends ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testCanDoUnitTest()
    {
        $this->assertTrue(true);
    }
}

</php>

4.  From the command line, run the tests.

`
Shane:tests shane$ phpunit --configuration phpunit.xml 
PHPUnit 3.4.6 by Sebastian Bergmann.

.

Time: 0 seconds, Memory: 9.25Mb

OK (1 test, 1 assertion)

Generating code coverage report, this may take a moment.
`

This just sets up the environment, writing tests is a whole 'nother world I need to explore. Also the topic of setting up the DB for use with tests.  stay tuned.

Resources:
http://blog.fedecarg.com/2008/12/27/phpunit-testing-zend-framework-controllers/
http://weierophinney.net/matthew/archives/190-Setting-up-your-Zend_Test-test-suites.html
http://akrabat.com/php/zend_loaders-autoloader_deprecated-in-zend-framework-18/#comment-27670
http://www.zendcasts.com/unit-testing-with-the-zend-framework-with-zend_test-and-phpunit/2009/06/

Was this helpful? Lets me know.
