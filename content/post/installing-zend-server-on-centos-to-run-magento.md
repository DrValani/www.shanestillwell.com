---
title: "Installing Zend Server on Centos to run Magento"
id: 62
date: "2009-07-21T17:09:27-05:00"
tags: 
- Server Admin
---

Installed Zend Server CE on one of my servers today. 

## Installing Zend Server CE via yum on Centos

To install via YUM, you can add the Zend repo as `/etc/yum.repos.d/zend.repo`
`
  [Zend]
  name=Zend CE $releasever - $basearch - Released Updates
  baseurl=http://repos.zend.com/rpm/ce/$basearch/
  enabled=1
  gpgcheck=0
  [Zendce-noarch]
  name=Zend CE - noarch
  baseurl=http://repos.zend.com/rpm/ce/noarch
  enabled=1
  gpgcheck=0
`

Then run `yum install zend-ce`

This will run a muck on your current website, it basically replaces your current php installation, replaces your mod_php with a zend.  The files for the Zend Server are stored in `/usr/local/zend`. You will need to restart apache to take advantage of the new Zend.

## Accessing the Zend Server admin interface

In a browser you just go to `<yourserver>:10081`, from there it will prompt you for a password, then pass you along to the Zend admin page.  From there you can enable caching/optimization. 

## Magento

I already had Magento installed, and Zend Server messed with some settings that caused my Magento to generate an error.

`
Can't connect to local MySQL server through socket '/tmp/mysql.sock'
`

This took me a while to figure out, it went around looking at the config files for Zend `/usr/local/zend/etc/conf.d/` that didn't seem to help. The fix was specifically telling Magento what unix_socket to use for the db connection.
Need to change local.xml
`

      <connection>
          <host><![CDATA[localhost]]></host>
          <username><![CDATA[YOURUSERNAME]]></username>
          <password><![CDATA[YOURPASSWORD]]></password>
          <dbname><![CDATA[YOURDBNAME]]></dbname>
          <unix_socket><![CDATA[/var/lib/mysql/mysql.sock]]></unix_socket>
          <active>1</active>
      </connection>
`

I enabled both the Zend Cache and the Zend Optimizer+ and saw some decent increases in performance.  Magento is a resource hog and you really need a beefy server to run it, utilizing cacheing is almost a must. 
