---
title: Reach Docker Host From Container
date: "2016-08-26T08:06:17-05:00"
tags:
- Docker
---

This is the best way I've found to set up for a container to contact the host machine.

```
sudo ifconfig lo0 alias 172.16.123.1
```

Now you can use the IP `172.16.123.1` to contact your local host machine. Might want to store that in an environment variable.

*Note: I had written a much longer, more indepth post, but a few unfortunate key strokes in Vim obliterated much of the post... <sigh>*
