---
title: "Poedit and phtml files giving error from xgettext"
id: 132
date: "2011-08-26T01:27:20-05:00"
tags: 
- Programming
---

### Problem

I had just set up a Poedit for a project at work and was trying to update the *.po file with new translations. It wasn't finding a whole load of them.

After a while of poking around, it dawned on me that it was not looking in my phtml files. Simple enough, I just added *.phtml to the list of extensions in the parsers preference.

Then I receive a big long error message that xgettext didn't know what phtml.

### Solution

Add pthtml to your list of extensions
```
*.php;*.phtml
```

You also need to add " -L php" to the end of your Parser command
```
xgettext --force-po -o %o %C %K %F -L php`
```
![Poedit Parse PHP](images/Screen-Shot-2011-08-25-at-9.15.19-PM.png "Poedit Parse PHP")
`
