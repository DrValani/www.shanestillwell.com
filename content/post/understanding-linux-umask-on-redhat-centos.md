---
title: "Understanding Linux umask on Redhat / Centos"
id: 101
date: "2010-04-06T17:58:25-05:00"
tags: 
- Server Admin
---

umask is linux's way of determining default file/folder permissions when new files and folders are created. It's a little odd in that the umask is subtracted from 777 for folders and 666 for files to get the correct permissions. I don't claim to understand fully, but it just is.  

What is your current umask?
`
umask
0002
`

By default, normal users will get a umask of 0002 = 775 on a folder and 664 on a file. This equates to <pre>-rwxrwxr--</pre> for a file. 

I had a user that had a set umask of 0022, but all other users had a umask of 0002\. This puzzled me, I looked in the normal areas that you can override the umask <pre>/etc/profile</pre> and <pre>~/.bashrc</pre> but didn't see anything obvious. Then looking in <pre>/etc/passwd</pre> I noticed this user had a groupid set to apache's group (48). This was done for reasons outside this discussion.

So if a userid or groupid &lt; 99 then umask = 0022 (more restrictive).

Resources:
http://kbase.redhat.com/faq/docs/DOC-1373
