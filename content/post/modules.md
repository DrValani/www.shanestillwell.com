---
title: "Modules"
id: 85
date: "2010-01-28T14:51:19-05:00"
tags: 
- Programming
---

Some of the plugins, extensions, or modules I've created for different projects.

## Drupal

### Jquery Slideshow

This module creates a slide showing from the imagefields of a node.  Each node is a separate slide show of the related images uploaded via imagefield.  The module relies on Imagecache to offer custom sized images.  This is the perfect module for allowing a visitor to create their own custom slideshow.

[Jquery Slideshow Project Page](http://drupal.org/projects/jquery_slideshow)
