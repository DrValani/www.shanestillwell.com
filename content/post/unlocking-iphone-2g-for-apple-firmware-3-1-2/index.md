---
title: "Unlocking iPhone 2G for Apple Firmware 3.1.2"
id: 90
date: "2010-02-03T23:43:13-05:00"
tags: 
- Apple
---

Here are some instructions for unlocking the Apple iPhone 2G model with firmware version 3.1.2\. I ran into a few problems, but having unlocked dozens of these from day one, I've yet to brick one.

1.  Download the firmware version from apple. You want the [**3.1.2** version](http://appldnld.apple.com.edgesuite.net/content.info.apple.com/iPhone/061-7268.20091008.32pNe/iPhone1,1_3.1.2_7D11_Restore.ipsw) 250MB
2.  Connect the phone to iTunes
3.  Hold down the OPTION button and click the "Restore" button. This will prompt you for a firmware file, located the file you just downloaded
4.  Sometimes the upgrade gives an error message after its done, I've yet to have a problem so you can safely ignore it if you get one.
5.  Download [blackra1n](http://www.blackra1n.com) by geohot
6.  Unzip and run blackra1n with the iPHone plugged into the computer.  It only takes a minute and after it reboots the iPhone will be jailbroken with a new app on the springboard
![IMG_0202.PNG](images/IMG_0202.png)
7.  Run blackra1n on the iPhone and install Cydia
8.  Run Cydia... it will update it self several times, you just want graphical mode and essential updates
9.  When this is done you'll want to install BootNeuter using Cydia

    ![BootNeuter2.png](images/BootNeuter2.png)

10.  Run the BootNeuter app and you'll see this screen, then click 'flash'. When I performed this, the phone seemed to freeze. I tried rebooting the phone, but it became 100% unresponsive. After 5 minutes it rebooted and it seems it worked.

    ![bootneuter.png](images/bootneuter.png)

11.  All this was done with one Sim, I switched to a different SIM and received this error message. "Different Sim Detected ... Connect to iTunes to activate" and the phone went to the pre-activated state.
12.  I took the SIM out and started from the beginning and this seemed to work
13.  Last if you don't have a data plan, you'll want to disable cellular data network with this Fake APN. Just visit this address in Safari on the iPHone and follow the directions to install the Fake APN. unlockit.co.nz

Credit:
http://www.redmondpie.com/jailbreak-unlock-iphone-2g-3.1.2-firmware-with-blackra1n-rc3-9140086/
http://www.ehphone.ca/2008/07/how-to-disable-data-on-your-iphone-or-iphone-3g/
