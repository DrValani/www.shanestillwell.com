---
title: Create a file in Vim
date: "2014-07-11T10:01:40-05:00"
tags:
- Vim
---
There are two primary was to work with the filesystem while in Vim. These two tools will allow you to create a new file, create a new directory, copy files, delete files and folders. Everything you need.

## The built in File Explorer.

To use this built in file explorer, just think `:Sex`, that's right Sex. This will open up the built in file explorer in a buffer and allow you to naviate around the filesystem. It can open up files on remote systems too by using SFTP, and other remote file reading tools. Some commands you can use

```
D      Delete a file/folder
R      Rename a file/folder
d      Create a directory
%      Create a folder
```

## A better way

If you've been using Vim for more than a day, you should have already installed the [NERDTree](https://github.com/scrooloose/nerdtree) plugin. It's pretty much indispensable. You can use NERDTree to navigate around the filesystem and open up files, but you can also, create, copy, move, and delete files and folders really easily with NERDTree.

So open the NERDTree sidebar, then put the cursor over a file and press the `m` key. A menu like the one below will appear at the bottom of the screen.

```
NERDTree Menu. Use j/k/enter and the shortcuts indicated 
==========================================================
> (a)dd a childnode
  (m)ove the current node
  (d)elete the current node
  (r)eveal in Finder the current mode
  (o)pen the current node with system editor
  (q)uicklook the current node
  (c)opy the current node
```

From here you can press `a` to add a file in the same directory. Type the name of the file and press enter. BAM! Try it out, you'll love it.

