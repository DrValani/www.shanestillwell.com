---
title: "Improving my Vim environment with ctags and Tagbar"
id: 476
date: "2012-08-11T17:31:01-05:00"
tags: 
- Vim
- Programming
---

I love Vim. There is something about it. Most people that "try" Vim, hate and wonder what all the fuss is about. If you force yourself to learn it, customize your environment and start using it, you'll want to use it for all your text editing needs. Here are few more improvements I've installed today.

&nbsp;

## Tagbar

[Tagbar](http://majutsushi.github.com/tagbar/) allows you to see inspect the current file listing the properties and methods in that file. This works for both **PHP** and **JavasScript** (yes more too I'm sure).

Here's how to install on OS X, I'm assuming you are using the wonderful [Pathogen](https://github.com/tpope/vim-pathogen/) to install Vim plugins
<pre>cd ~/.vim/bundles
git clone https://github.com/majutsushi/tagbar.git
echo "nmap  :TagbarToggle" &gt;&gt; ~/.vimrc</pre>
Restart Vim, open a file in Vim and then press **F8**. That will open a window on the right of your screen showing _Classes_, _Functions_, and _Variables_. Clicking (or moving cursor over top of them and hitting enter) will move the cursor in the other window to the location.
[![](/images/uploads/2012/08/Screen-Shot-2012-08-11-at-12.00.52-PM-1024x622.png "Screen Shot 2012-08-11 at 12.00.52 PM")](http://www.shanestillwell.com/images/uploads/2012/08/Screen-Shot-2012-08-11-at-12.00.52-PM.png)

## Ctags

Ctags allow you to jump to the file of the classes you are calling, this was the one feature I really missed that PHPStorm did very well.

First you need to install ctags, I would recommend installing [Mac Ports](http://www.macports.org/), then installing ctags with
<pre>port install ctags</pre>
After this you can generate your ctags for a project with
<pre>cd /your/project/directory/
ctags -R --languages=php .</pre>
Now you when the cursor is over a class name you can click **Ctrl+]** to jump to that file and **Ctrl+T** will jump back to the original file.

**Simply Awesome.**

## Resources:

[http://www.zalas.eu/jumping-to-a-class-function-and-variable-definitions-in-vim-with-exuberant-ctags
http://haridas.in/vim-as-your-ide.html ](http://www.zalas.eu/jumping-to-a-class-function-and-variable-definitions-in-vim-with-exuberant-ctags )
