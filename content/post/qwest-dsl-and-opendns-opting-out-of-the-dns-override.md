---
title: "Qwest DSL and OpenDNS... opting out of the DNS override."
id: 129
date: "2011-07-25T23:52:11-05:00"
tags: 
- Server Admin
---

While reconfiguring the network at the church, I noticed that my DNS queries were not getting curbed by OpenDNS.  Browsing to 'bad' sites should give you a no no page from OpenDNS, but nothing was tripping it... not even 4chan :S

The I realized that Qwest is doing some DNS override where they actually do the DNS query.  

## The Solution

You need to opt out of this "wonderful" service from Qwest.
http://www.qwest.net/web.help/?confirm=1

After that, it was just fine.

I guess Qwest couldn't resist trying to pick up that money on the table since they have access to all that traffic.... wonder how much money they make on ads.
