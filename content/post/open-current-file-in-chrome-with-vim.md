---
title: Open current file in Chrome with Vim
date: "2013-12-05T09:08:22-05:00"
tags:
- Vim
---

Most of the time when I'm working on a Markdown document like a README, I want to preview it in Chrome as markdown. Perhaps I'm working on an HTML document and want to preview it in Chrome. It would be a real pain to go to Chrome and then look for the file I'm working on. Luckily Vim makes this really easy.

## The Setup

Add this to your [.vimrc](https://github.com/shanestillwell/dotfiles/blob/master/vimrc)

```
" Open up current file in chrome
nmap <silent> <leader>ch :exec 'silent !open -a "Google Chrome" % &'
```

(Optional) Add a Markdown viewer to Chrome. I use [Markdown Preview Plus](https://chrome.google.com/webstore/detail/markdown-preview-plus/febilkbfcbhebfnokafefeacimjdckgl).

Now in Vim you just need to enter these commands in Normal mode (hint: my `<leader>` is `,`)

```
,ch[Enter]
```

That's all there is to this. It allows you to simply open the current file in Chrome.

## References
* [My vimrc](https://github.com/shanestillwell/dotfiles/blob/master/vimrc)




