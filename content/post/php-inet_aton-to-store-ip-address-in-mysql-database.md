---
title: "PHP inet_aton to store IP address in MySQL Database"
id: 116
date: "2010-09-03T02:17:56-05:00"
tags: 
- MySQL
- Programming
---

Don't store IP addresses in a database as a string like (192.168.10.10). That is just the human readable form of that number. You want to store it in the database as an integer.

MySQL has built in functions to handle converting a dot-notation ip address to an integer equivalent.

`
mysql&gt; SELECT INET_ATON('192.168.0.10') AS ipn;
+------------+
| ipn |
+------------+
| 3232235530 |
+------------+

mysql&gt; SELECT INET_NTOA(3232235530) AS ipa;
+--------------+
| ipa |
+--------------+
| 192.168.0.10 |
+--------------+
`
But, I needed this type of functionality in PHP itself. Long story short, I'm using [Doctrine](http://www.doctrine-project.org/projects/orm/1.2/docs/manual/introduction/en#introduction "Doctrine ORM") to handle the relationship that PHP has to the database and I need to convert the human readable dot-notation IP address to the integer style (unsigned integer that is).

PHP's built in function [ip2long](http://www.php.net/manual/en/function.ip2long.php) comes close, but there is a little bit more to work the magic.

`

$ip = long2ip(ip2long("192.168.1.10"));
print sprintf("%u", ip2long($ip));

`
<pre>sprintf</pre>
is used to convert the signed integer to an unsigned one.

Ready to be inserted into the DB.
