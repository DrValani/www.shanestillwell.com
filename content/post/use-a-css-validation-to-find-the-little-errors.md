---
title: "Use a CSS Validation to find the little errors"
id: 53
date: "2009-05-21T18:22:38-05:00"
tags: 
- CSS
- Design
---

Just an FYI, note to myself, use a css validator to catch little CSS problems that will most likely choke IE6.

[http://jigsaw.w3.org/css-validator/](http://jigsaw.w3.org/css-validator/)
