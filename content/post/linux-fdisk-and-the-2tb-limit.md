---
title: "Linux fdisk and the 2TB limit"
id: 97
date: "2010-03-13T03:24:03-05:00"
tags: 
- Server Admin
---

Running out of space on the backup drive, I added another 1.5TB drive to the existing one to hold the company backup files.  We do rsync style snapshots with a linux server and it was at 80% capacity. So I added another Seagate SATA to the simple Hardware RAID SATA card in the machine. Everything went well.  Ran `fdisk` to partition the drive, ran `mke2fs -f /dev/hde1` to format in EXT3 format. After that was done the <pre>df -h</pre> command showed only 2TB. That's odd, I know the filesystem takes some drive space, but not 1TB of it.

So after a little investigating you need to use a program called <pre>parted</pre> for drives >2TB. The commands are as follows

<pre>
parted /dev/hde1
</pre>

Once in the parted command prompt then you can run these commands on the new drive.

<pre>
mktable gpt
mklabel 
mkpart primary 0 100%
quit
</pre>

You can now format with <pre>mke2fs -j /dev/hde1</pre>

References:
http://ubuntuforums.org/archive/index.php/t-901368.html
