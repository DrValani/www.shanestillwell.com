---
title: "Zend Framework Application Progress"
id: 76
date: "2010-01-05T05:04:18-05:00"
tags: 
- Programming
---

Today I learned from Zend Framework.

## Redirecting visitors to their intended URI after login

`LoginForm.php`
<php>
  ...
  // Grabs the requested URI from the server and stores it in a hidden field
  $this->addElement('hidden', 'referrer', array('value' => $_SERVER['REQUEST_URI']));
  ...
</php>

Then after you complete authentication, send them to the referring URI
<php>
  if($form->getValue('referrer'))
      $this->_redirect($form->getValue('referrer'));
  $this->_redirect('index/index');
</php>

Credits:
http://stackoverflow.com/questions/1249274/redirect-to-previous-page-in-zend-framework

* * *

## Throwing an error message

This one is easy. You want to throw an error message with some meaning.
<php>
  throw new Zend_Exception('This is the error message');
</php>

* * *

## Saving data in Zend_Session

You want to save some data in a session variable so you can access it other places in application. So in the `Bootstrap.php` file you would place
<php>
  Zend_Session::start();
</php>

To set the variable you would use
<php>
  $ns = new Zend_Session_Namespace('hello');
  $ns->domain = "Hello World";
</php>
By using the namespace "hello" or whatever you want, then you can keep your variables separate from another part of the applicate that just might happen to name it's variable "domain" as well.

To retrieve this fun filled variable.
<php>
  $ns = new Zend_Session_Namespace('hello');
  print $ns->domain;
</php>
