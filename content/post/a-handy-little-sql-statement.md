---
title: "A handy little SQL statement"
id: 73
date: "2009-12-31T11:22:40-05:00"
tags: 
- SQL
- Programming
---

Have a look at the following SQL

<sql>
SET @salt=SUBSTRING(MD5(RAND()),-20);
</sql>
This will set a variable called `@salt` that has a random string of 20 characters.
