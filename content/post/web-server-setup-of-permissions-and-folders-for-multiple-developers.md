---
title: "Web Server Setup of Permissions and Folders for multiple developers"
id: 108
date: "2010-05-20T20:28:23-05:00"
tags: 
- Server Admin
---

This summarizes how I currently set up my servers so that I can have multiple developers accessing certain client files, but retain control on which clients. The key to this whole process is groups.

## Users Create user accounts for your developers.

```
# useradd developer1
# passwd developer1
# useradd developer2
# passwd developer2
```

## Groups Create groups that section off your clients. For example.

```
# groupadd client1
# groupadd client2
```

### Add apache to your groups The web server is going to need access to the files so we will add apache to any groups we create for this purpose.

```
# usermod -a -G client1 apache
# usermod -a -G client2 apache  
```

This will add apache to the groups client1 and client2.

### Add developers to your groups Your developers need to access your client files as well so we assign them to the groups. This will assign `developer1 -> client1` and `developer2 -> client2`. Fun stuff.

```
# usermod -a -G client1 developer1
# usermod -a -G client2 developer2  
```

Note: the `-a` appends, if you don't have this option, then it overwrites the users groups list, not good. You could also section off your groups in other ways, such as by website.

## DocumentRoot Folders

### The web server folders will be located at  `/var/www/`. So you will have :

```
/var/www/client1
/var/www/client2
```

### Setting the setgid bit

```
# chmod 2770 /var/www/client1
# chmod 2770 /var/www/client1
```

### Assigning User and Group ownership to the directories

This will have to be done by root, because you cannot assign ownership to another person unless you are root. Also, I created a dummy user of "www" to be the user owning the files, it doesn't matter who owns the files because we are not relying on that part of the permissions.

```
# chown www:client1 /var/www/client1
# chown www:client2 /var/www/client2  
```

The permissions on these folders will now look like

```
#ls -l /var/www/
drwxrws---  6 www client1 4.0K Apr 14 15:28 client1
drwxrws---  6 www client2 4.0K Apr 14 15:28 client2
```

This will ensure that all files and folders created below this directory

## Result

Developer1 can access Client1's files, change them, create new folders/files, but cannot get to Client2's files. Developer2 has the visa-vers result. **NOTE**: any new file created by the developers will be owned by the developer and the group (e.g. the ownership will be `developer1:client1`). This is not a problem since we are not really checking user level permissions.

## Helpful commands

### Set the setgid on all folders.

```
# find ./* -type d -print0 | xargs -0 chmod 2775  
```

### Set all files to appropriate permissions

```
# find ./* -type f -print0 | xargs -0 chmod g+rw
```
