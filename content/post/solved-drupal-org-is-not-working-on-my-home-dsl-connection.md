---
title: "[Solved] Drupal.org is not working on my home DSL connection."
id: 44
date: "2009-03-29T02:05:17-05:00"
tags: 
- Programming
---

I've had this problem for at least a week. I can resolve the name drupal.org, I can even connect, but I don't get any response.  I telnet'd to port 80 for drupal.org to try it manually and it just hangs.  Does DO hate me?

`
Shane:~ shane$ telnet drupal.org 80
Trying 140.211.166.6...
Connected to drupal.org.
Escape character is '^]'.
get /index.php
`

It just sits there after I give the `get /index.php` command.

So to resolve this I set up a SOCKS proxy using SSH the command is 

`
$ssh -D 9999 username@ip-address-of-ssh-server
`

After this you just set Firefox to use a SOCKS proxy (not sure which version).

[Update] The solution was changing my Actiontec modem from PPPoE to PPPoA.
