---
title: "custom-modules"
id: 86
date: "2010-01-28T14:53:06-05:00"
tags: 
- Programming
---

A collection of custom modules, extensions, and plug-ins I've developed for various projects.

## Drupal

### Jquery Slideshow

[Jquery Slideshow Project Page](http://drupal.org/projects/jquery_slideshow)
