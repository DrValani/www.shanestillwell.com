---
title: "Spamassassin upgrade caused error of \"exited or timed out without...\""
id: 105
date: "2010-04-22T00:47:06-05:00"
tags: 
- Server Admin
---

I just upgraded Spamassassin, and low, after I restarted it I received an error message while trying to start Spamassassin.
<pre>[root]# /etc/init.d/spamassassin start
Starting spamd: child process [21162] exited or timed out without signaling production of a PID file: exit 255 at /usr/bin/spamd line 2588.
                                                           [FAILED]</pre>
Per the article below, a simple
<pre>sa-update</pre>
at the command line updated the Spamassassin rules. Then it would start like normal.

Resources:
http://www.elehost.com/faq/web-tool-tips-and-fixes/54-spamassassin/172-fix-for-sa-spamd-error-qchild-process-exited-or-timed-out-without-signaling-production-of-a-pid-fileq
