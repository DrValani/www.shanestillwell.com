---
title: "The disappointment of cordless mice"
id: 70
date: "2009-12-03T17:56:18-05:00"
tags: 
- Design
---

I've test driven several cordless mice in the last few years. All of them [Logitech](http://www.logitech.com/ "Logitech") because I'm biased. I really, really, hate cords. They just clutter the desk area up and seem to be less important as time goes on. I really thought that the cordless mice had "arrived" and were just as good as their corded counterparts. Wrong. While most applications will find a wireless mouse to meet their needs just fine, I'm on the computer a solid 8-10 hours a day, little things add up and turn into a big deal. Here they are in order of usage.

## Logitech Wireless MX500 Cordless

![](http://www.logitech.com/repository/450/gif/3442.1.0.gif)
This is going back 6 years to my first wireless mouse. Not sure what kind of signaling technology it uses, but it has a dongle with a charging station built in for the mouse's rechargeable batteries. This was a work horse and was used on both Windows and Linux. I loved setting the buttons on the thumb to copy/paste.  Eventually the little pads on the bottom of the mouse fell off, then the connectors on the charging station didn't make good contact to charge the mouse (I sort of fixed this). After I gave this mouse up, it's hopped to a few different people and is still in operation today. A tank of a mouse.

## Logitech v270 Cordless Optical Mouse for Bluetooth

![v270 cordless](http://www.logitech.com/repository/303/jpg/2153.1.0.jpg)
I choose this one because of the smaller design, the built in bluetooth (the Macbook Pro has built in Bluetooth, no adapter needed). I liked that I didn't have to plug anything into my laptop, but the optical was not very sensitive on my desk and at this point I didn't know if the lag was because of the optical led or the bluetooth.

## Logitech V450 Nano Cordless Laser Mouse for Notebooks (Silver)

![](http://www.logitech.com/repository/194/jpg/2185.1.0.jpg)
I choose this mouse because of the laser and it didn't have Bluetooth. It requires a small dongle, that conveniently is stored in the mouse itself and uses the same wireless spectrum as Wifi. This was a good little mouse but suffered from the same problem, once in a while it was slightly unresponsive and lagged a little. I still use this mouse at work and does a great job overall. 

## Logitech v470 Cordless Laser Mouse for Bluetooth

I choose this as an upgrade to the v270 because this has laser precision and bluetooth. It still suffered from the lag, but not as bad as the optical mouse. So the general rule of thumb is stay away from the optical mice, go with laser.
![](http://www.logitech.com/repository/515/jpg/4430.1.0.jpg "Logitech v470")

Back to a corded mouse

## Corded Mouse M500

![](http://www.logitech.com/repository/1671/jpg/21794.1.0.jpg)
This is a basic laser corded mouse. I've only had it a few days, but the lag is gone and the tracking is good. The superfast scroll wheel is convenient, but too sensitive to leave on all the time. I've gotten out of the habit of using my thumbs on the mouse so I don't use the side buttons yet. 

## Summary

Part of the problem may be that the Bluetooth antenna in the Macbook Pro is not placed very well. There is probably a lot of interference in there, but I'm not going to buy a Bluetooth dongle to place the antenna in a better location, that kind of defeats the purpose of a wireless mouse. I've gone back to a wired mouse and it's not such a big deal since I now use a USB hub for all my devices (External Hard Drive, iPhone, Mouse). 

**I still prefer the keyboard to the mouse, but in these evil times, a mouse is a necessity.**
