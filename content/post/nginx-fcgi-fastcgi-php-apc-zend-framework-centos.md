---
title: "Nginx + fcgi (fastcgi) + PHP + APC + Zend Framework + CentOS"
id: 123
date: "2011-01-18T04:31:30-05:00"
tags: 
- Server Admin
---

My newest toy I've been playing with is Nginx. Getting PHP to work over FastCGI.  The performance gain over Apache + mod_php is quite staggering. 

Here are a few things to watch out. 

## Getting PHP v5.2

Some of my apps are not ready to make the jump to PHP 5.3 just yet, so I needed to get a hold of PHP 5.2\.  In the past I just install the [Zend Server CE](http://files.zend.com/help/Zend-Server-Community-Edition/rpm_installation.htm) it would have all the PHP bells and whistles I could ever need.  Not anymore, now it's time to fend for myself.

**Webtatic Repo** 
http://www.webtatic.com/blog/2009/06/php-530-on-centos-5/
I've added webtatic to my repos and followed his advice to disable PHP 5.3 from his repos you add `exclude=php*5.3*` to your `webtatic.repo` file.

Now you can install PHP.

## APC Opcode Cache

This waa a little more of struggle as I need to install various things to get it to work. I finally ran `pecl install apc` and it compiled fine.  Then I added a file 

/etc/php.d/apc.ini
```
extension=apc.so
```

PHP will pick that up and just enable it.  Make sure to check that it's enabled by running `phpinfo()`.  Also you can download the source and extract the file `apc.php` Throw this on your server and run it in the browser, it will give you a nice rundown about the state of APC and let you know if it's working.

## PHP via FastCGI

There are a few ways to accomplish this and much debate on which is the best way, but I followed the instructions on 
http://www.cyberciti.biz/faq/rhel-fedora-install-configure-nginx-php5/

It lays it out plain, how to get a FastCGI daemon going.  One change I did make though.  It has the daemon listening on localhost:9000\. I changed my to a socket located at `/tmp/phpcgi.socket` 

Here is the `/etc/init.d/php_cgi` init script.

```

#!/bin/sh
#
# php-cgi - php-fastcgi swaping via  spawn-fcgi
#
# chkconfig:   - 85 15
# description:  Run php-cgi as app server
# processname: php-cgi
# config:      /etc/sysconfig/phpfastcgi (defaults RH style)
# pidfile:     /var/run/php_cgi.pid
# Note: See how to use this script :
# http://www.cyberciti.biz/faq/rhel-fedora-install-configure-nginx-php5/
# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

# Check that networking is up.
[ "$NETWORKING" = "no" ] && exit 0

spawnfcgi="/usr/bin/spawn-fcgi"
php_cgi="/usr/bin/php-cgi"
prog=$(basename $php_cgi)
server_ip=127.0.0.1
server_port=9000
server_user=nginx
server_group=nginx
server_childs=5
pidfile="/var/run/php_cgi.pid"
socket="/tmp/phpcgi.socket"

# do not edit, put changes in /etc/sysconfig/phpfastcgi
[ -f /etc/sysconfig/phpfastcgi ] && . /etc/sysconfig/phpfastcgi

start() {
    [ -x $php_cgi ] || exit 1
    [ -x $spawnfcgi ] || exit 2
    echo -n $"Starting $prog: "
    #daemon $spawnfcgi -a ${server_ip} -p ${server_port} -u ${server_user} -g ${server_group} -P ${pidfile} -C ${server_childs} -f ${php_cgi}
    daemon $spawnfcgi -s ${socket} -u ${server_user} -g ${server_group} -P ${pidfile} -C ${server_childs} -f ${php_cgi}
    retval=$?
    echo
    return $retval
}

stop() {
    echo -n $"Stopping $prog: "
    killproc -p ${pidfile} $prog -QUIT
    retval=$?
    echo
    [ -f ${pidfile} ] && /bin/rm -f ${pidfile}
    return $retval
}

restart(){
	stop
	sleep 2
	start
}

rh_status(){
	status -p ${pidfile} $prog
}

case "$1" in
    start)
        start;;
    stop)
        stop;;
    restart)
        restart;;
    status)
        rh_status;;
    *)
        echo $"Usage: $0 {start|stop|restart|status}"
        exit 3
esac

```

## Zend Framework

One issue I ran into with Zend Framework, was the location of the sessions folder and the permissions.  The default session folder is in `/var/lib/php/session` this was owned by apache, I just changed the owner:group to nginx:nginx. All was sunshine and roses again.
