---
title: "Deploying Drupal via CVS (including modules)."
id: 95
date: "2010-03-03T07:31:13-05:00"
tags: 
- Programming
---

If you're not using version control for your software projects please turn in your geek badge at the door. Seriously, version control makes life so much easier when developing features for a site or deploying a site.  The best version control out there currently would have to be [Git](http://git-scm.com/ "Git"), [subversion](http://subversion.tigris.org/) a distant second, and coming in last the old and painful [CVS](http://en.wikipedia.org/wiki/Concurrent_Versions_System). Drupal is currently on CVS, but migrating to Git, we'll have to deal with CVS for now.

**Needless to say, these have to be run from the command line**

## Step One: Installing the Core

In the directory that you would normally download and unzip drupal you'll want to run this command to grab Drupal core from the CVS server

`
cvs -z6 -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal co -r DRUPAL-6-14 drupal
`That will grab

*   Drupal version 6.14
*   place all the files in a directory called 'drupal'

Simple, fast, painless.

## Step Two: Installing Modules via CVS

Every install of Drupal needs [CCK](http://drupal.org/project/cck) and [Views](http://drupal.org/project/views). So here are the commands to install each of them. Now you will want to navigate to your `drupal/sites/all/modules` directory. From there run the following commands.

This will checkout cck version 6.x-2.6
`
cvs -z6 -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib checkout -r DRUPAL-6--2-6 -d cck contributions/modules/cck
`

This will checkout views version 6.x-2.8
`
cvs -z6 -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib checkout -r DRUPAL-6--2-8 -d views contributions/modules/views
`

I think you get the picture. Continue this as you see fit.

**To see the module version in Drupal you will need to also install the [CVS Deploy](http://drupal.org/project/cvs_deploy) module. Very important**

## Step 3: Upgrade Time

### Core

So after a while you have to upgrade Drupal core you simply navigate to your root drupal directory and run this command.

`
cvs update -dP -r DRUPAL-6-15
`That will make all the necessary changes to bring Drupal from 6.14 -> 6.15

### Modules

For modules you'll want to navigate inside the module directory and run the update command, for example for cck you would go to `drupal/sites/all/modules/cck` and run

`
cvs update -r DRUPAL-6--2-7 -dP
`That will upgrade CCK from 2.6 -> 2.7

## Step 4: Eat some chocolate ice cream
