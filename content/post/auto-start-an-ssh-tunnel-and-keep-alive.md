---
title: "Auto start an SSH tunnel and keep alive"
id: 100
date: "2010-03-30T21:36:34-05:00"
tags: 
- Server Admin
---

I have a program that needs to talk to another server, but to secure the traffic I've set up a port forwarding SSH tunnel.  The only problem is that this tunnel needs to be kept alive and started when the server boots up.  Here is how, using <pre>/etc/inittab</pre>

For the server you want to make connections from follow these instructions.

Open up <pre>/etc/inittab</pre> and insert this code somewhere near the bottom
`
# Keeps an SSH port forwarding connect between serverA <---> serverB for mysql
sm:345:respawn:/usr/bin/ssh -N -L 3307:127.0.0.1:3306 -l admin 192.168.1.5
`
Let's break it down
<ul>
<li>**sm** This is just an random two letter code that distinguishes it from other processes inside inittab</li>
<li>**345** These are run-levels that you want the process to run.</li>
<li>**respawn** What to do if the process dies, respawn it</li>
<li>**/usr/bin/ssh** ssh binary</li>
<li>**-N** Tells SSH not to run any remote command after the connection has been established</li>
<li>**-L 3307:127.0.0.1:3306** This tells SSH to set up a tunnel with local port being 3307, remote host 127.0.0.1, remote port 3306</li>
<li>**-l admin** What user to log in by</li>
<li>**192.168.1.5** Remote host to SSH into</li>

For those familiar with SSH, it should go without saying that you need to set up pre-shared keys to automatically log into the remote server 
