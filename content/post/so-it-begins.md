---
title: "So it begins..."
id: 19
date: "2008-06-24T14:16:12-05:00"
tags: 
- Good Ideas
---

Some good quotes I've read today

"A little learning is a dangerous thing" -Alexander Pope

"Everyone can be taught to sculpt, Michelangelo would have to be taught how not to."

Now go tackle your day.
