---
title: "dstat to rule them all (iostat, vmstat, blah, blah)"
id: 54
date: "2009-06-09T04:20:19-05:00"
tags: 
- Server Admin
---

I could never remember what stat to use when trying to figure out disk usage and network usage, but now there is a dstat that handles all that.  Great!

![dstat](images/Screen shot 2010-01-02 at 3.59.13 PM.png)
