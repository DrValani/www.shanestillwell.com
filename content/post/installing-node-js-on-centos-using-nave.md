---
title: "Installing Node.js on CentOS using Nave"
id: 592
date: "2012-12-04T14:17:51-05:00"
tags: 
- Node.js
- Server Admin
---

So I needed to install Node.js on CentOS, but I was hard pressed to find a yum repo that was up to date. Then I stumbled upon [Nave](https://github.com/isaacs/nave). This is really easy. It's just a bash script that you run to install node and npm. 
    # Get the nave.sh shell script
    wget https://raw.github.com/isaacs/nave/master/nave.sh
    chmod +x nave.sh
    ./nave.sh usemain 0.8.8
    node -v

    # Will print
    # v0.8.8

## Life could not be easier
