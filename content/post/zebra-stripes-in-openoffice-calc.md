---
title: "Zebra Stripes in OpenOffice Calc"
id: 22
date: "2008-06-27T14:56:22-05:00"
tags: 
- Apple
---

[Open Office](http://www.openoffice.org/ "") is a great alternative to Microsoft Office.  It does lack in some areas where as MS Office might provide an easy way to accomplish a task, OpenOffice can usually do it with a little bit of acrobatics.

Zebra Striping in Calc
----------------------

1\. With Calc opened up, view the Styles and Formatting window (Format > Styles and Formatting)
2\. Right click in the empty white space of that box and select "New"
3\. Name the new cell style "odd"
  1\. Change the background color of the cell to a color of your desire
  2\. Click OK
4\. Repeat step #2 and create a style called "even"
  1\. Change the background color to a different color of your choice
  2\. Click OK
5\. Select the range of cells that you want to zebra strip
6\. Click Format > Conditional Formating...
7\. Change the values in that box to match these
![](/images/uploads/conditional_formating.png)
8\. The brains of this operation is the `MOD(ROW();2)=0` and `MOD(ROW();2)=1`
9\. To tweak the formating just edit the styles "odd" and "even"
