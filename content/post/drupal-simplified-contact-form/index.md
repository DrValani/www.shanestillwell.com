---
title: "Drupal Simplified Contact Form"
id: 93
date: "2010-02-13T22:16:43-05:00"
tags: 
- Programming
- Drupal
---

A site I'm working on has a simple contact us form in the footer of every page. I didn't want the long drawn out contact form with all the name, subject, email, message fields. Here is what I did.

## Installed the [formblock](http://drupal.org/project/formblock) module

Easy enough, install the module, enable the block so that the contact form now shows in the footer.

## Simplify the contact form

Now we want to hide some of the fields in the contact form.

In your `template.php` file, you'll want to add the following lines.

```php
<php>
function basic_theme(){
  return array(
    'contact_mail_page' => array(
      'arguments' => array('form' => NULL)
      )
    );
}

/**
 *  Hide and autofill some fields in the contact form.
 *
 */

function basic_contact_mail_page($form){

  $form['subject']['#value'] = "Website question";
  $form['subject']['#type'] = 'hidden';
  $form['name']['#type'] = 'hidden';
  $form['name']['#value'] = 'Website Customer';
  return drupal_render($form);
}
</php>
```

As you can see I'm using the [Basic](http://drupal.org/project/basic) theme from Drupal. If you're using a different theme you'll want to change the word `basic` to your own theme (e.g. `blueprint`).

Now you have a simple contact form.

![drupalcontact.png](images/drupalcontact.png)

## Real World Example

http://www.pineriversales.com/
