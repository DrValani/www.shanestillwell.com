---
title: "SVN ignore in all subdirectories"
id: 50
date: "2009-05-05T23:33:03-05:00"
tags: 
- SVN
- Programming
---

`svn propset svn:ignore '*' .`
[http://www.heavymind.net/2007/08/29/the-nightmare-has-returned-with-new-name-svnignore/](http://www.heavymind.net/2007/08/29/the-nightmare-has-returned-with-new-name-svnignore/)

<div class="zemanta-pixie">![](http://img.zemanta.com/pixy.gif?x-id=3d5fa3b2-f78e-8a39-a7c6-f0682a708da8)</div>
