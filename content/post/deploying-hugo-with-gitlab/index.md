---
date: 2017-01-12T16:19:07-06:00
description: Deploying a Hugo site with Gitlab CI
title: Deploying a Hugo site with Gitlab CI
tags:
- AWS
- Git
- Gitlab
---

As a hip, cutting edge developer, you're using [Hugo](https://gohugo.io/) to generate your blog or website. Wordpress... pssh, you're way too cool for that.

Being cool has its complications though. How and where should you deploy your Hugo app. Should you push it locally using rsync or some other fancy means? Whatever.

This simple post is going to show you how to deploy your Hugo app using Gitlab's Continous Integration feature. Something I've fallen in love with as I've understood how to set up builds and push out my code to production. I'll be using **Cloudfront/S3** to host my Hugo site. You can checkout the following links to get more familiar with how it works.

* [A Handy Publish Script for Static Sites](/2016/08/21/a-handy-publish-script-for-static-sites-using-cloudfront/)
* [Using Free AWS SSL with Cloudfront/S3/Route53](/2016/02/14/using-free-ssl-and-cloudfront-for-an-angular-react-site/)

## Gitlab Setup steps

1. First you need to [set up an account with Gitlab](https://gitlab.com/users/sign_in) if you have not already.
1. Then you need to [create a project](https://gitlab.com/projects/new).
1. Next you'll need to add Gitlab as a remote to your git files (where you currently have your cool Hugo app).
1. Last bit is to set up some Variables for your app. To deploy to S3 and fire off a Cloudwatch invalidation request, Gitlab is going to need your `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`. You'll store these in the Gitlab variables (hint on the project page, click the gear drop down and select Variables).
  ![Variables](images/Screen Shot 2017-01-12 at 4.33.21 PM.png)

## The Gitlab CI file .gitlab-ci.yml

You'll tell the pipeline how to operate by including a `.gitlab-ci.yml` file in the root of your project. Ready. This is what it looks like.

```yaml
stages:
  - build
  - deploy

build:
  services:
    - docker:dind
  image: docker:latest
  stage: build
  script:
    - docker run -v `pwd`:/src northernv/hugo hugo
  artifacts:
    paths:
      - public/

deploy:
  stage: deploy
  image: garland/aws-cli-docker
  variables:
    S3_BUCKET_NAME: "www.example.com"
    DISTRO_ID: "__YOUR_CLOUDFRONT_DIST_ID__"
  script:
  - aws configure set preview.cloudfront true
  - aws s3 sync ./public s3://$S3_BUCKET_NAME --delete;
  - aws cloudfront create-invalidation --distribution-id $DISTRO_ID  --paths "/*";
  dependencies:
  - build
```

The only items you need to change are the variables in the file for `S3_BUCKET_NAME` and `DISTRO_ID`.

This uses a Docker image I build which you can see the guts is described in this [Dockerfile](https://gitlab.com/shanestillwell/docker-hugo/blob/master/Dockerfile)

That's it. Commit, push, and in a few minutes your Hugo app will be published out for the world to bask in your awesomeness.
