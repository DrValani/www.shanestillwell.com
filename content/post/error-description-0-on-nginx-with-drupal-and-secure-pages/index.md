---
title: "Error Description: 0 on Nginx with Drupal and Secure Pages"
id: 124
date: "2011-03-02T14:27:19-05:00"
tags: 
- Server Admin
- Nginx
- Drupal
---

A curious error message popped up after I moved my [Drupal](http://drupal.org) sites to [Nginx](http://wiki.nginx.org/Main). It would only happen on AJAX type changes, but not all of them. I first noticed it when I went to change the author of a node. Then when I tried to change a View I received this error message.

![](images/Screen_shot_2011-03-02_at_8-thumb_23_44_AM.png)
After checking the logs on Nginx and watching what happens using Firebug's Net panel, it occurred to me that Drupal was making an HTTP "**OPTIONS**" request. 

![](images/Screen_shot_2011-03-02_at_8-thumb_24_55_AM.png) The other detail is I was using HTTPS (SSL) enforced with the [Secure Pages](http://drupal.org/project/securepages) module. It when I was on the HTTPS side of the site those AJAX requests would be HTTP OPTIONS requests to the non-secure side HTTP. 

### The Solution

 The solution was to disable HTTPS for certain pages in the Secure Pages config settings. Why Drupal was making OPTIONS calls to the none secure side of the site is still a mystery. 

BTW. When your on the non-secure side, it makes a regular GET or POST request, not an OPTIONS request.
