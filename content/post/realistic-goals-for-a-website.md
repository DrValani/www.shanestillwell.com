---
title: "Realistic Goals For a Website"
id: 36
date: "2008-10-06T13:39:32-05:00"
tags: 
- Programming
---

Don't go half-cocked looking for a website when you haven't addressed the following issues. I get far too many clients that 'want a website' but have no clue what they want the website to do for them, or even if it serves a purpose.

## Purpose

What is the website going to do? Sell products? Give potential customers hours of operation and location information? Helpful tips and handy how-to's that will display your expertise? A website is just like other parts of your business, it must fulfill a business need or it's useless. If you do not dedicate some time and resources to this question, then it's best if you just wait on the website. A better idea is to contact me about getting listing on Google Maps and other free advertising that takes little effort on business owner's part.

## End Result

Tied into the purpose of the website is the end result. Identify these with your developer before you lay the first brick. You don't build a house without seeing what it looks like at least on paper. Have a vision in your mind of what a good and a bad end result of your website will be. How it will flow, the feel you want it to give.  Answer this, "When someone visits my website they should...?" Call you for a quote? Email you their pictures? Tell a friend about you? What is the action associated with your product.

## Realistic Goals

You are not going to be making $50,000 in the first month only working part-time. Leave that to the people on the info-mercials. Remember too that the websites you are most familiar with (e.g. Amazon, eBay, Google, etc) have million dollar budgets for their site.  Your budget is a little more restricted. Also, start small. It's OK to go live with just one or two pages on your website telling your customers where you are, how to find you, and how to contact you. That's a good start.

You are not going to be on the first page of coveted key terms like money, construction, computers, wood working, etc. Besides why would you want to be, you want qualified leads, not general shoppers. For example, if you operate a store, you want people in your store buying things, not just picking things up, looking at the price and leaving. Think about what a failure it would be as a business owner if the real shoppers couldn't walk into your store because the window shoppers were too many and crowding them out.

## When will it be a success

Define when your website will have reached a successful launch. Is it when you get so many customers per day? Is that realistic? When you start showing up on Google searches Is that goal realistic? When it looks good? 

If you do not define when you have attained victory, then the project will continue to go on and on with no end in sight. Clear goals and definitions of success will help you plan better.

## Understanding the pitfalls

That is where I can help. Pitfalls in design can keep your site from being indexed by Google properly. Pitfalls in accessibility can keep people with disabilities from accessing yours site properly. Pitfalls in domain name ownership and management could mean headaches down the road. Let the Northern Voyageur help you avoid these common pitfalls and more.
