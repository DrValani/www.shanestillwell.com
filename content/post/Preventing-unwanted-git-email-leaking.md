---
title: Preventing unwanted git email leaking
date: "2016-03-31T07:02:50-05:00"
tags:
  - Git
---

Maybe you work on different git projects, on business, then at home for personal projects. In the past, as a convenience, I set my `~/.gitconfig` to include my name and email like the following.
<!--more-->

```
[user]
	name = "Shane A. Stillwell"
	email = "shanestillwell+spam@gmail.com"
```

This is exactly what git wants you to do when it detects you haven't set your name or email 

```
*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.
```

## So what's the big deal?

Now when you are working on a projects for work, or different clients, it will use the global name / email, but shoot, you didn't mean to make that commit with your personal email, it was supposed to be your work email. Bummer, it's now a permanent part of git log.

## The solution

Very simple. Open up your `~/.gitconfig` and change your email to *none*. (I assume your name doesn't change between projects)

```
[user]
	name = "Shane A. Stillwell"
	email = "(none)"
```

Now in each project, before you can commit you'll be prompted like above, just remember **NOT** to use the `--global` flag, e.g. `git config user.email "shanestillwell+spam@gmail.com"`. Now each git repo will have a correctly set email address and you're less likely to leak personal emails into business projects.


