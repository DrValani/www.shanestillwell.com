---
title: Instant Messanger Clients Compared
date: "2013-11-23T11:19:38-05:00"
tags:
---

Instant messangers are so vital to communcation on the web these days, both in and out of an organization. Let's take a look at some of the popoular ones out there with their pros and cons. As we compare them, we'll consider cost, features, portability, and easy of use.

## Google Hangouts
Gtalk has been sort of reborn as Hangouts. This web based solution has even just recently had another update that makes it very appealling for group video chats. With hangouts you can contact anyone that has a Google account. Hangouts also supports third party pluggins that help do things like record a video chat session or have your name appear below your video like a news caster does on TV. Screen sharing in Hangouts works great while on a video call.

### Pros
* Ubiquity of the Google network (many people have at least one Google Account)
* Great video chats right in your browser
* Free

### Cons
* Poor support for chat rooms and being able to see the history of a chat room. 
* Video chat is browser based, this is a con in my opinion


## Campfire
Campfire was developed by 37Signals and integrates well with their products. Campfire was very early on, offering web based chat. You can drag and drop images right into the chat and they are preserved there for reference. I'll be honest, I've only really played with Campfire

### Pros
* Web client

### Cons
* No voice or video, just plain old text chat

## HipChat
HipChat and Campfire have a lot in common, but HipChat goes well beyond Campfire. HipChat has a web client as well as native clients for Windows / OS X / iPhone / Android. 

### Pros

### Cons
* It's easy to miss undread messages as it does not track these like other instant messaging service.

## Skype

### Pros

### Cons


