---
title: Reformat with Tabs for Vim
date: "2014-11-12T11:09:16-05:00"
tags:
---

While I'm not a big fan of TABS in my source code, a recent project I'm on has declared tabs are the way to go. So I needed to figure out two things. 

1. How do I tell Vim to use TABS on this project, but not on other project's I'm on?
2. How do I reformat some of my code to be tabs instead of spaces?

## Using directory specific `.vimrc` file 

This one was easy. Go into your global `.vimrc` file and enter this 

```
set exrc " Per directory .vimrc file
```

Now in the root directory of your project, you will add a `.vimrc` file with these contents

```
set noexpandtab
set copyindent
set preserveindent
set softtabstop=0
set shiftwidth=4
set tabstop=4
```

NOTE: You may want to add `.vimrc` to your global `.gitignore` file so you don't accidentally include it in your code :)


## Reformatting code to use tabs instead of spaces  

This one is already built right in. Just highlight a section of text in visual mode. Then type `:retab!` this should swap tabs for spaces. Try it out and tell me what you think.
