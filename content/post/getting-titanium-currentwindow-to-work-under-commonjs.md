---
title: "Getting Titanium CurrentWindow To Work Under CommonJS"
id: 498
date: "2012-09-06T18:35:59-05:00"
tags: 
- JavaScript
- Programming
---

Appcelerator is a moving target, developed and changing pretty fast. The _old way_ was using the **url: ** property of a window to open up a new screen with a javascript screen. 

### Old and Busted

```javascript
var window = Ti.UI.createWindow({
    url: 'screen.js'
});
```

### Another Old Way

Then came the [Tweetanium](https://github.com/appcelerator-titans/tweetanium) structure, while a solid way to set up, it's not the route Appcelerator is recommending anymore.

See my post for more information on the [Tweetanium Setup](/index.php/2012/03/29/mvc-for-appcelerator-titanium-understanding-tweetanium/)

### The New Hostness

Appcelerator is now recommending the CommonJS route just like that found in Node.js. But you'll find [all](http://developer.appcelerator.com/question/5391/currentwindow) [kinds](http://developer.appcelerator.com/question/118956/iphone-currentwindow-not-working) of [people](http://developer.appcelerator.com/question/6581/is-tiuicurrentwindow-broken) not able to find the current window. 

### How do you find the currentWindow?

First I set up a globals.js module that I'll use to hold global information.

```javascript
/**
 * Global options
 *
 * @author Shane A. Stillwell
 */

var globals = {
    currentWindow: null
};

exports.getOption = function(key) {
    return globals[key];
};

exports.setOption = function(key, value) {
    globals[key] = value;
    return;
};

exports.getAll = function() {
    return globals;
};
```

Then I need to add event listeners when the window changes, I'm currently using a TabGroup for this project, so right after I declair a TabGroup I attach an eventlistener it.

```javascript
// Require the globals.js file (it'll be a singleton)
var globals = require('/modules/globals');

//create base proxy object and component wrapper
var self = new Ti.UI.createTabGroup();

// Add event listener to set the currentWindow in globals
self.addEventListener('focus', function(e) {
    Ti.API.info(e.tab.getWindow());
    globals.setOption('currentWindow', e.tab.getWindow());
});
```

Now when I want to know the current window I just call this

```javascript
// Require the globals.js file (it'll be a singleton)
var globals = require('/modules/globals');

var currentWindow = globals.getOption('currentWindow');
```

Now I can remove child views, close, animate, etc. whatever
