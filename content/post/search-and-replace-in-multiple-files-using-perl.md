---
title: "Search And Replace in Multiple Files Using Perl"
id: 4
date: "2006-12-10T05:20:28-05:00"
tags: 
- Programming
---

`
perl -pi -w -e 's/search/replace/g;' *.php
`
 -e means execute the following line of code.
-i means edit in-place
-w write warnings
-p loop

Example I had the following style sheet in a section:

and I wanted the following instead:

As each expression is a regular expression you've got to escape the special characters such as forward slash and .
\.\.\/includes\/style\.css

So the final line of code ends up as
perl -pi -w -e 's/\.\.\/includes\/style\.css/admin\.css/g;' *.php 
