---
title: iTerm open tab in current working directory
date: "2013-12-03T15:26:25-05:00"
tags:
- Apple
---

I can't believe I lived so long without looking for a fix to a common issue I have while in [iTerm2](http://www.iterm2.com/#/section/home). Maybe I'm running some [GruntJS](http://gruntjs.com/) task that is updating the screen, but I want to do something in the CWD. So I would open another tab `⌘ + t`, then `cd ...` to my cwd. 

Instead just tell iTerm you want new tabs to open in the CWD.

* Open up iTerm2 preferences `⌘ + ,`
* Go to _Profiles > General_
* Now click on _Edit_ under _Advanced Configuration_ for **Working Directory**
  ![iterm](images/iterm1.png "")
* Now just select when you want it to open in the same directory like so
  ![iterm](images/iterm2.png "")


Now you have an extra 10 seconds a day to do with whatever you want.... live the freedom.
