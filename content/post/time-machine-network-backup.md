---
title: "Time Machine Network Backup"
id: 47
date: "2009-04-03T18:41:10-05:00"
tags: 
- Programming
---

Fate has not blessed you with a tower of money such as Uncle Scrooge, so a Time Capsule is not an option, but you have a network share either at work or home you can. Enter the Time machine hack that will let you backup your Mac to a network share.

<span style="font-weight: bold;">Pros</span>

*   No need to plug in an external drive to your Macbook Pro
*   You can back up multiple computers to the same file server
*   Utilize the power of Wifi<span style="font-weight: bold;">Cons</span>

*   A little difficult to get up and running
*   Speed is an issue compaired to a FireWire 800 attached drive
*   Others may be able to access your data

### Setting Up a TimeMachine Network Backup

1.  Find out the name of your Mac and make sure it only has letters and numbers. Get rid of any (') or other unneeded characters. Warning - This could mess with other things you have set up.

    `System Preferences &gt; Sharing`
2.  Find out your Mac's MAC address. Pull up the Terminal application and paste the following code in to get the MAC address our your Mac.

    `ifconfig en0 | grep ether`

3.  The name of the file you are going to create in Step 4 will be &lt;name&gt;_MacAddress.sparsebundle. For example shane_0016cbaf91d7.sparsebundle

4.  With the Terminal still open.&nbsp; Type in `cd` then type in the following, remember to adjust the size and the name of the file.

    `
hdiutil create -size 140g -fs HFS+J -volname "Backup of Shane" shane_0016cbaf91d7.sparsebundle
`

5.  Now place this sparsebundle file in the root directory of the share you want to back up to.
6.  You need to enable Time Machine to see the network backups. Paste the following in the Terminal and run it

    `defaults write com.apple.systempreferences TMShowUnsupportedNetworkVolumes 1`

7.  Now mount the share with the Finder &gt; Go &gt; Connect To Server, the format for a Windows share (or Samba)

    cifs://&lt;domain_name&gt;;&lt;username&gt;@&lt;hostname&gt;/&lt;sharename

8.  Open up Time Machine preferences. Select 'Change Disk', Select your network share. Time Machine should prompt you for your username/password on that share, you'll want to have it remember that info by checking the box.
9.  Time Machine will start to backup in 2 minutes and will mount the sparsebundle as a drive in the Mac Finder.&nbsp; There is no need to be connected to share all the time as Time Machine will connect to it and mount the sparse bundle automatically.
10.  One you see this sparse bundle mounted, you need to go to System Preferences &gt; Spotlight &gt; Privacy. Click the '+' and add the mounted drive to the list. We do not want Spotlight to find info on this drive... it will slow things down.

### References

[http://www.readynas.com/?p=253](http://www.readynas.com/?p=253)

<div class="zemanta-pixie">![](http://img.zemanta.com/pixy.gif?x-id=606feac3-0b53-8d94-a37d-2e23b421b351)</div>
