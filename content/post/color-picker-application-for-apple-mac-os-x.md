---
title: "Color Picker Application for Apple Mac OS X"
id: 27
date: "2008-07-30T14:52:12-05:00"
tags: 
- Apple
---

To create websites and graphics being able to pick colors is important.  Many times you see a great color and you want to sample and get the [HEX](http://en.wikipedia.org/wiki/Web_colors#Hex_triplet "Web colors - Wikipedia, the free encyclopedia") number of the color to use.  In the past I've used the [ColorZilla](http://www.iosart.com/firefox/colorzilla/ "ColorZilla Extension for Firefox and Mozilla") extension for [Firefox](http://www.mozilla.com/firefox/ "Firefox web browser | Faster, more secure, &amp; customizable").  This worked very well, has many options, but one draw back, you can only sample colors in Firefox.

Then I stumbled across a [post](http://www.macosxhints.com/article.php?story=20060408050920158&amp;lsrc=osxh "macosxhints.com - Make the OS X Color Picker into an application") on [Mac OS X Hints](http://www.macosxhints.com/ "macosxhints.com"). You can make the Mac Color Picker a stand alone application. Great! This allows you to organize colors using various methods.

1\. Open the Apple Script Editor (Applications -> AppleScript -> Script Editor)
2\. Enter this text:
  `choose color`
3\. File > Save As. Make sure to choose Application from the File Format drop down.
4\. I saved mine under Applications > Utilities

Download the [HEX Color Picker](http://wafflesoftware.net/hexpicker/ "Hex Color Picker") and follow the directions in the README file to install it to the Color Picker. Then [top it off](http://support.apple.com/kb/HT2493?viewlocale=en_US "Mac 101: Change Your Icons") with a custom [ICNS](/files/blog/color1.icns) icon to make it look pretty. 
