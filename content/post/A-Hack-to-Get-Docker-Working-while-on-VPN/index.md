---
title: A Hack to Get Docker Working While on VPN
date: "2016-04-11T09:49:32-05:00"
tags:
  - Docker
---

**See the [improved version](/2016/04/26/A-Better-Hack-to-Get-Docker-and-VPN-to-Play-Nice)**

> If you've found this article, then you've banged your head against the problem of being on a restrictive VPN and using Docker at the same time. The culprit is usually Cisco AnyConnect or Junos Pulse.

## The Problem

You use [Docker](https://www.docker.com/) for development. For various reasons you need to connect to a VPN, but as soon as you do, Docker stops working. There are many solutions out there, some work, others do not. The bottom line is there is no elegant solution and this solution here is not elegant, but it will work. What's happening? Yeah, when you connect, AnyConnect blunders in, overwrites all your computer's routes to send them through the VPN tunnel. Luckily, it doesn't route localhost (127.0.0.1) to the tunnel. This is our backdoor to hack ourselves in.

## The Setup

My current setup involves using Docker Machine to create a [Parallels](https://www.parallels.com/) VM. I'm on a Mac, Window/Linux YMMV. VirtualBox should work just fine; VMWare, can't really say. Some really restrictive VPN that doesn't allow split traffic, like Cisco AnyConnect or Junos Pulse.

## The Hack

You'll want to setup your Docker Machine first and get your env setup `eval $(docker-machine env)`. Once you have your docker machine up. You'll want to set up a *Port Forwarding* rule in Parallels. Go to **Preferences > Networking**. Then you'll want to add a new rule like this

!['Port Forwarding Rule](images/Screen-Shot-2016-04-11-at-10.11.56-AM.png)

"default" is the name of my VM


Now on the command line, you need to update your **ENVIRONMENT VARIABLES** to use this new localhost incantation. We'll be changing the `DOCKER_HOST` and `DOCKER_TLS_VERIFY`. We set `DOCKER_HOST` to your localhost version. Then we need to disable TLS verification with `DOCKER_TLS_VERIFY`.

```
export DOCKER_HOST=tcp://127.0.0.1:2376
export DOCKER_TLS_VERIFY=""
```

Now you can connect to your restrictive VPN\* with `docker --tlsverify=false ps`.

![docker ps](images/Screen-Shot-2016-04-11-10-25-33.png)


This is not an elegant solution, but will work until I figure something else more robust.

## Caveats

1. You should have your VM up and running and have Docker-Machine env set in your terminal
1. You'll get numerous warnings from docker-compose, annoying, but they are just warnings.
1. You have to include `--tlsverify=false` with every Docker command e.g. `docker --tlsverify=false ps`

## Notes

1. Please keep in mind, companies implement restrictive VPN because it would be easy for a hacked computer or maliciously setup computer to allow access the VPN from outside world. By forwarding all ports through the VPN, it makes this security hole much more difficult.
1. I've tried going the route of readding the routes (pun intended) to the Mac's routing table to redirect the IP that Parallels VM is on back to the Parallels interface, but didn't get anywhere with that.
1. A better solution would be to include 127.0.0.1 with the SSL cert that Docker Machine creates for the VM, then you wouldn't have issues when connecting via 127.0.0.1

