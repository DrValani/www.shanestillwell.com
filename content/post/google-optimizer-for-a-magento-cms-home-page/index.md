---
title: "Google Optimizer for a Magento CMS Home Page"
id: 102
date: "2010-04-09T20:06:13-05:00"
tags: 
- Programming
- Magento
---

This will guide you on how to set up Google Optimizer in Magento for the Home page. It's a little confusing, but isn't everything when you first encounter it?

## Enable Google Website Optimizer in the Magento Configuration

This is located under System &gt; Configuration &gt; Google API &gt; Google Website Optimizer. Change "Enable" to Yes, then click "Save Config"
![Screen shot 2010-04-09 at 9.39.15 AM.png](images/Screen shot 2010-04-09 at 9_39_15 AM.png)

## Create Variant of your Home Page

In Magento go to:
CMS &gt; Manage Pages

Select Your Home Page

Copy the content.

Go Back to the list of CMS pages, and now you'll want to create a new page. Give it the same title, a different SEF URL ID (e.g. home2), change status to Enabled, then paste in your original's code.

Change the home page variant any way that you like, this is the variant that Magento and Google will alternate between.

## The Catch22

While you're in the Variant Home Page click on the "Page View Optimization" tab. For the conversion page select 'One Page Checkout'. This will give you the 'Conversion Page URL' that you'll need to copy and paste into the Google Website Optimizer during setup.

## Sign up for Google Website Optimizer

Go to the URL:
https://www.google.com/analytics/siteopt/splash?hl=en

Sign in with your Google Account.

Get started with an experiment. Name the experiment, place your two different home page URLs to test, paste in the 'Conversion Page URL'
![Screen shot 2010-04-09 at 9.51.23 AM.png](images/Screen shot 2010-04-09 at 9_51_23 AM.png)

Click Continue. You want to select 'Your webmaster will install and validate JavaScript tags.' and copy the URL it gives you.
![Screen shot 2010-04-09 at 9.59.22 AM.png](images/Screen shot 2010-04-09 at 9_59_22 AM.png)

## Back in Magento

Now back in Magento, for the Variant page you want to be on the Page View Optimization tab and paste in the Scrips Install URL. Select the Page Type to be Variant Page. Then click 'Install Scripts'. Some javascript will fill in below. Save this page.

Now go to the Original CMS Home Page.
Paste the Scripts Install URL, select Original Page for type and then click 'Install Scripts'

## Flip Back to Google Website Optimizer

Now in Google Website Optimizer click 'Check Status'. It should work and now can click 'Continue' and then enable that test.

Test the pages to make sure they look like, if you're like me I forgot to set the Layout for the variation page and it really looked bad. From here you can enable the test.

Hey, you're done. Now go do something useful for a change :)

Resources:
http://www.google.com/support/forum/p/websiteoptimizer/thread?tid=50461df6cfaeb689&amp;hl=en
