---
title: "Getting Out of the Vim Rut"
date: 2018-04-18T07:28:26-04:00
draft: true
tags:
- Vim
---

I admit it. I'm addicted to Vim. I can't explain it. Maybe because I'm driven to efficient and productive practices, it appeals to this bent of mine. To the outsider, Vim is some ancient dying practice only held on to by programmers of old.

I need to get out of my Vim rut.

To do this, I watched this video, that got me more motivated.

* The thing that stuck out most is the phrase "get angry at repeating keystrokes"
* Write stuff down and practice learning them.... deliberate

Then I decided I was going to improve, that's how it's always done (I made a decision).

1. Get faster at jumping around.
http://vim.wikia.com/wiki/All_the_right_moves

2. Learn the plugins you have , install new ones that make your life easier.

3. Write out the commands you want to learn



References.
* Better, faster, stronger
* Ruby Railsberry Vim
* 7 habits of text edit http://www.moolenaar.net/habits.html
