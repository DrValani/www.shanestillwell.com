#!/bin/bash
VERSION=${1:-0.47}

open http://localhost:1313;
docker run --rm -v `pwd`:/src -p=1313:1313 northernv/hugo:$VERSION hugo server --bind 0.0.0.0 -D;
